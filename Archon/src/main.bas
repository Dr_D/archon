#define FBMLD_NO_MULTITHREADING
'#include once "fbmld.bi"
#Include once "GL/gl.bi"
#include once "GL/glu.bi"
#include once "GL/glext.bi"
#include once "display.bi"
#include once "fbpng.bi"
#include once "fbgfx.bi"
#include once "math/mathf.bi"
#include once "math/vecf.bi"
#include once "math/matrix4x4f.bi"
#include once "chipmunk/chipmunk.bi"
#include once "model.bi"
#include once "physics.bi"
#include once "gfx.bi"

const FPS = 30.0
const FRAME_STEPS = 5

const num_of_max_nebulas = 6
const num_of_max_stars =  500
const num_of_big_stars = 99
const max_num_of_projectiles = 50
const max_num_of_comets = 5
const max_num_of_particles = 1000


const ScreenWidth = 1920'640'1024
const ScreenHeight = 1080'480'768
const fullscreen = 1
const ScreenXMid = ScreenWidth/2
const ScreenYMid = ScreenHeight/2

const as single GRAVITY = 0'9.8

'it's ok to use shared variables for stuff like this next set,
'but you don't want to use them on major things, like character arrays
dim shared as integer map_width, map_height, count_start_particle = 1
dim shared as single zoom, camera_centerx, camera_centery

dim shared as gluint star_tex(1 to 3) ' texture holders for stars
dim shared as gluint particletex      ' and the single particle
dim shared as gluint explode_tex

dim shared as particle_struct particles(1 to max_num_of_particles)    ' particles array


declare function init_physics_world( byref world as entity_struct ) as integer
declare sub entity_add_object( byref world as entity_struct, plr as entity_struct,  byref script_file as string, byref is_world as integer )
declare sub destroy_entity( byref world as entity_struct, byref plr as entity_struct )
declare sub entity_control( plr() as entity_struct, byref loop_time as double )
declare sub update_simulation( byref fSteps as integer, byref dt as single, byref World as entity_Struct, plr() as entity_struct )
declare sub update_entity( byref plr as entity_struct, player as integer )
declare sub update_projectiles( projectiles() as projectile_struct, byref loop_time as double )
declare sub update_comets( comets() as projectile_struct, byref loop_time as double )
declare function main( byval argc as integer, byval arv as zstring ptr ptr ) as integer


declare sub load_projectiles( byref World as entity_struct, projectiles() as projectile_struct )
declare sub load_comets( byref World as entity_struct, comets() as projectile_struct )
declare sub launch_projectile( byref plr as entity_struct, byref position as vec2f, byref direction as vec2f, byref angle as single, byref impact_force as single )
declare function ParseScriptLine ( script_file_line as string ) as string 
declare sub DrawShips ( plr() as entity_struct, cmat as matrix )
declare sub DrawPlanets ( world as entity_struct, plr() as entity_struct )
declare sub DrawNebula ( nebula() as sprite_struct, plr() as entity_struct, Layer_Speed as single, Layer_Transparency as single)
declare sub DrawStars ( star() as sprite_struct,  plr() as entity_struct, starset as integer, layer_speed as single, layer_transparency as single)
declare sub ResetParticle( particle() as particle_struct, byval num as integer, byval clr as integer, byval xDir as single, byval yDir as single, byval plife as single)
declare sub InitiateProjectile ( projectile() as particle_struct, byval typ as integer, byval clr as integer, xpos as double, ypos as double, byval xDir as single, byval yDir as single, byval plife as single )
declare sub InitiateParticle ( particles() as particle_struct, byval typ as integer, byval clr as integer, byval xpos as double, byval ypos as double, byval xDir as single, byval yDir as single, byval plife as single )
declare sub DrawParticles (particles() as particle_struct, plr() as entity_struct, byref loop_time as double)
declare sub Draw_Projectiles ( projectiles() as projectile_struct, plr() as entity_struct, byref loop_time as double)
declare sub Draw_Comets ( comets() as projectile_struct, plr() as entity_struct, byref loop_time as double)

'gl sprite wrapper functions for lachie
declare function Load_Texture( byref Texture as Gluint, byref filename as string, byref filter as integer ) as integer
declare function Get_Sprite_From_Texture( byref source as gluint, byref start_x as integer, byref start_y as integer, byref get_width as integer, byref get_height as integer, byref src_width as integer, byref src_height as integer ) as gluint

'start the main function of the program, with advanced debugging support
end main( __FB_ARGC__, __FB_ARGV__ )


function main( byval argc as integer, byval arv as zstring ptr ptr ) as integer
    
    randomize timer
    dim as entity_Struct world
    dim as entity_struct plr(1 to 2)
    dim as sprite_struct nebula(1 to num_of_max_nebulas)
    dim as sprite_struct star(3, num_of_max_stars)
    dim as projectile_struct projectiles( 1 to max_num_of_projectiles )
    dim as projectile_struct comets( 1 to max_num_of_comets )
    dim as gluint quad, flist, fonttex
    dim as gluint temp_texture
    dim as vec2f Look_Targ, look_center
    dim as double loop_time, this_time, last_switch, fps_timer
    dim as single font_scale
    dim as integer png_height, png_width, frames_ps, do_draw = 1 ' do_draw is for debugging physics speed
    dim as string png_name, fps_string
    
    map_width = 4500
    map_height = 3500
    
    Open_GL_Window( ScreenWidth, ScreenHeight, 32, 0, 0, fullscreen )
    font_scale = Display.W / 80
    make_font_dlist( fList, 16, 16, font_scale )
    if Load_Texture( fonttex, "data/gfx/text.png", TEX_NEAREST ) <> 0 then
    end if
    
    init_physics_world( world )
    
    if Load_Texture(Temp_Texture, "data/gfx/particle.png", TEX_NEAREST ) <> 0 then
        'Image loading failed... try something else! :p
    end if
    ParticleTex = Get_Sprite_From_Texture( Temp_Texture, 0, 0, 32, 32, 32, 32)    
    glDeleteTextures( 1, @Temp_Texture )
    
    if Load_Texture(Temp_Texture, "data/gfx/smoke1.png", TEX_NEAREST ) <> 0 then
        'Image loading failed... try something else! :p
        beep
    end if
    explode_tex = Get_Sprite_From_Texture( Temp_Texture, 0, 0, 256, 256, 256, 256)
    glDeleteTextures( 1, @Temp_Texture )
    
    
    if Load_Texture( Temp_Texture, "data/gfx/stars.png", TEX_NEAREST ) <> 0 then
        'Image loading failed... try something else! :p
    end if
    for count_star as integer = 1 to 3
        star_tex(count_star) = Get_Sprite_From_Texture( Temp_Texture, (count_star-1)*16, 0, 16, 16, 64, 16 )
    next count_star
    glDeleteTextures( 1, @Temp_Texture )
    
    for count_nebula as integer = 1 to num_of_max_nebulas
        
        png_name = "data/gfx/nebula_spr"+ltrim(rtrim(str((count_nebula))))+".png"
        png_dimensions( png_name, png_width, png_height )
        
        if Load_Texture(Temp_Texture, png_name, TEX_MIPMAP ) <> 0 then
            'Image loading failed... try something else! :p
        end if
        
        nebula(count_nebula).sprite = Get_Sprite_From_Texture( Temp_Texture, 0, 0, png_width, png_height, png_width, png_height)    
        
        glDeleteTextures( 1, @Temp_Texture )
        
        select case count_nebula
        	case 1
        		nebula(count_nebula).position = vec2f( 1000, 1200 )
            case 2
                nebula(count_nebula).position = vec2f( 2200, 1800 )                
            case 3
                nebula(count_nebula).position = vec2f( 1000, 2800 )
            case 4
                nebula(count_nebula).position = vec2f( 1500, 2500 )
            case 5
                nebula(count_nebula).position = vec2f( 3000, 500 )
            case 6
                nebula(count_nebula).position = vec2f( 3000, 2000 )
        end select
        
        
    next count_nebula
    
    for countstar as integer = 1 to num_of_max_stars-(num_of_big_stars+1)
        
        Star(1,countstar).position.X = int(rnd * map_width) + 1 
        Star(1,countstar).position.Y = int(rnd * map_height) + 1
        Star(1,countstar).Typ = int(rnd * 3) + 1
        
        select case Star(1,countstar).Typ 
            case 1
                Star(1,countstar).MColor = &H8E9B9BF0
            case 2
                Star(1,countstar).MColor = &H9FABABF0
            case 3
                Star(1,countstar).MColor = &HB3BDBDF0
        end select
        
    next countstar
    
    for countstar as integer = num_of_max_stars-num_of_big_stars to num_of_max_stars
        
        Star(1,countstar).position.X = int(rnd * map_width) + 1 
        Star(1,countstar).position.Y = int(rnd * map_height) + 1
        Star(1,countstar).Typ = int(rnd * 3) + 4
        
    next countstar
    
    for countstar as integer = 1 to num_of_max_stars-(num_of_big_stars+1)
        
        Star(2,countstar).position.X = int(rnd * map_width) + 1 
        Star(2,countstar).position.Y = int(rnd * map_height) + 1
        Star(2,countstar).Typ = int(rnd * 3) + 1
        
        select case Star(2,countstar).Typ 
            case 1
                Star(2,countstar).MColor = &H738383DC
            case 2
                Star(2,countstar).MColor = &H869696DC
            case 3
                Star(2,countstar).MColor = &H616F6FDC
        end select
        
    next countstar
    
    for countstar as integer = num_of_max_stars-num_of_big_stars to num_of_max_stars
        
        Star(2,countstar).position.X = int(rnd * map_width) + 1 
        Star(2,countstar).position.Y = int(rnd * map_height) + 1
        Star(2,countstar).Typ = int(rnd * 3) + 4
        
    next countstar
    
    for countstar as integer = 1 to num_of_max_stars
        
        Star(3,countstar).position.X = int(rnd * map_width) + 1 
        Star(3,countstar).position.Y = int(rnd * map_height) + 1
        Star(3,countstar).Typ = int(rnd * 3) + 1
        select case Star(3,countstar).Typ 
            case 1
                Star(3,countstar).MColor = &H738383B4
            case 2
                Star(3,countstar).MColor = &H869696B4
            case 3
                Star(3,countstar).MColor = &H616F6FB4
        end select
        
    next countstar
    
    
    'add the first planet to the world object from a script file
    entity_add_object( world, world,  "data/planet1" & "_script.txt", 1 )
    
    'add another planet the the world for testing... works great, btw. :p
    entity_add_object( world, world,  "data/planet2" & "_script.txt", 1 )
    
    dim as string fname
    
    'add player 1's ship randomly
    fname = "data/ship" & 3 & "_script.txt"
    entity_add_object( world, plr(1),  fname, 0 )
    plr(1).pptr = @projectiles(1)
    plr(1).object[0]->tech_id = 1
    
    'add player 2's ship randomly
    fname = "data/ship" & 3 & "_script.txt"
    entity_add_object( world, plr(2),  fname, 0 )
    plr(2).pptr = @projectiles(1)
    plr(2).object[0]->tech_id = 2
    
    load_projectiles( world, projectiles() )
    load_comets( world, comets() )
    
    'what this next part is doing, is telling shipmunk that we want to run a certain function,
    'when a ship collides with another ship
    'this function i have named as "ship_Ship_Collision_Callback()"
    'it resides in "physics.bas"
    'and it's declared in "physics.bi" for our convenience ;)
    'we will take care of advanced physical properties in this callback function
    'for instance, we can make shields glow, take damage, etc...
    cpSpaceAddCollisionPairFunc( World.Space, SHIP, SHIP, @Ship_Ship_Collision_Callback(), 0 )
    
    'for this, it's the same as above, but this is the function that gets called when,
    'a ship collides with the terrain... in our case, the planet. ;)
    cpSpaceAddCollisionPairFunc( World.Space, SHIP, TERRAIN, @Ship_Terrain_Collision_Callback(), 0 )
    
    
    cpSpaceAddCollisionPairFunc( World.Space, SHIP, PROJECTILE, @Ship_Projectile_Collision_Callback(), 0 )
    
        
    'we are now starting the main loop
    'setting the current loop time now,
    ' will keep the first loop from going wacko
    'if we don't do this, there will be a huge timestep on the first loop
    this_time = timer
    fps_timer = timer + 1
    do
        
        
        if multikey( FB.SC_I ) then
            for i as integer = 1 to 2
                destroy_entity( world, plr(i) )
                fname = "data/ship" & 1 + int(rnd*5) & "_script.txt"
                entity_add_object( world, plr(i),  fname, 0 )
                sleep 20, 1
            next
        end if
        
        'for our time based movement/animation stuff
        loop_time = timer-this_time
        this_time = timer
        
        frames_ps+=1
        if this_time>fps_timer then
            fps_string = "FPS " & frames_ps
            fps_timer = this_time+1
            frames_ps = 0
        end if
        
        'run the simulation loop for the physics world
        'and apply external(controllable) forces to all bodies where needed
        dim as single dt = 1.0/FPS/FRAME_STEPS
        update_simulation( FRAME_STEPS, dt, World, plr() )
        
        
        for i as integer = 1 to ubound(plr)
            update_entity( plr(i), i )
        next        
        
        update_entity( world, 0 )
        
        update_projectiles( projectiles(), loop_time )
        
        update_comets( comets(), loop_time )
        
        entity_control( plr(), loop_time )
        
        ' get ship 1 and ship 2 positions
        dim as vec2f p1 = plr(1).object[0]->get_position
        dim as vec2f p2 = plr(2).object[0]->get_position
        
        dim as integer xdistance, ydistance, distance
        
        ' calculate the x and y distances between ships 
        XDistance = p1.X-p2.X
        YDistance = p1.Y-p2.Y
        
        ' depending on ship distances take the map edges into
        ' conideration to pick the right midpoint (edgeless map effect)
        if p1.X < map_width/2 and p2.X > map_width / 2 and abs(p1.X-p2.X) > abs(p1.X-(p2.X - map_width)) then XDistance = p1.X-(p2.X-map_width)
        if p1.X > map_width/2 and p2.X < map_width / 2 and abs(p1.X-p2.X) > abs((p1.X-map_width) - p2.X) then XDistance = (p1.X-map_width)-p2.X
        if p1.Y < map_height/2 and p2.Y > map_height / 2 and abs(p1.Y-p2.Y) > abs(p1.Y-(p2.Y - map_height)) then YDistance = p1.Y-(p2.Y-map_height)
        if p1.Y > map_height/2 and p2.Y < map_height / 2 and abs(p1.Y-p2.Y) > abs((p1.Y-map_height)- p2.Y) then YDistance = (p1.Y-map_height)-p2.Y
        
        ' calculate the absolute distance between ships	
        distance = sqr((XDistance)^2 + (YDistance)^2)
        ' calculate the zoom from the distance
        zoom = 2.0 / distance * 200
		
        ' zoom limiters (can be adjusted)
        if zoom > 1 then zoom = 1
        if zoom < 0.4 then zoom = 0.4
        
        ' Calculate the camera position (my vodoo)
        Camera_CenterX = -(p1.X - ScreenXMid/zoom - XDistance/2)
        Camera_CenterY = -(p1.Y - ScreenYMid/zoom - YDistance/2)
        
        dim as matrix cam_mat
        
        cam_mat.LoadIdentity()
        set_ortho( display.w, display.h )
        
        ' Scale the camera matric according to zoom.
        cam_mat.Scale( zoom, zoom, 1f )
        ' Translate the point of view to camera center (expressed as negative value).
        cam_mat.Translate( Camera_CenterX, Camera_CenterY, 1f )
        
        glClear( GL_COLOR_BUFFER_BIT ) ' Clear screen
        
        glEnable( GL_BLEND ) ' Enable default blending...
        'glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
        glBlendFunc(GL_SRC_ALPHA,GL_ONE)
        
        if do_draw = 1 then
            DrawStars (star(), plr(), 3, 1.8, 0.7) ' draw stars layer 3, distance 1.8, alpha 0.7
            DrawStars (star(), plr(), 2, 1.6, 0.8) 
            DrawStars (star(), plr(), 1, 1.3, 0.9)
            
            DrawNebula (nebula(), plr(), 1.3, 0.8) ' draw the nebula layer, distance 1.3, alpha 0.8
            
            DrawShips (plr(), cam_mat) ' draw ships using the camera matrix
            Draw_Projectiles ( projectiles(), plr(), loop_time)
            draw_comets( comets(), plr(), loop_time )
            
            drawplanets( world, plr() )
            DrawParticles (particles(), plr(), loop_time)
            
        end if
        
                
        if plr(1).object[0]->collided <> 0 or plr(2).object[0]->collided <> 0 then
            plr(1).object[0]->collided = 0
            plr(2).object[0]->collided = 0
            glprint( "COLLISION!!!", fonttex, fList, 0, display.h-font_scale-80, 1, 1, 0 )
        end if    
        
        
        'if plr(1).object[0]->taken_damage <> 0 or plr(2).object[0]->taken_damage <> 0 then
        '    plr(1).object[0]->taken_damage = 0
        '    plr(2).object[0]->taken_damage = 0
        '    glprint( "DAMAGE!!!", fonttex, fList, 0, display.h-font_scale-100, 1, 1, 0 )
        '    beep
        'end if
        
        glprint( "Ship 1 pos: " + str(p1.x)+","+str(p1.y), fonttex, fList, 0, display.h-font_scale, 1, 1, 0 )
        glprint( "Ship 2 pos: " + str(p2.x)+","+str(p2.y), fonttex, fList, 0, display.h-font_scale-20, 1, 1, 0 )
        
        glprint( fps_string, fonttex, fList, 0, display.h-font_scale-40, 1, 1, 0 )
        drop_ortho()
        flip
        sleep 1,1
        
    loop until multikey(FB.SC_ESCAPE)
    
    
    for i as integer = 1 to ubound(plr)
        destroy_entity( world, plr(i) )
    next
    destroy_entity( world, world )
    
    for i as integer = 1 to max_num_of_projectiles
        deallocate( projectiles(i).shape )
        unload_model( projectiles(i).model )
    next
    
    for i as integer = 1 to max_num_of_comets
        deallocate( comets(i).shape )
        unload_model( comets(i).model )
    next
    
    cpSpaceFree( World.Space )
    print #debug, "ALL CLEAR ON EXIT!!!"
    sleep 1000,1
    close #debug
    return 0
    
end function


sub load_projectiles( byref World as entity_struct, projectiles() as projectile_struct )
    dim as vec2f tpoints(1 to 3)
    
    for i as integer = 1 to max_num_of_projectiles
        
        with projectiles(i)
            .mass = .0001
            
            if Load_Texture( .sprite, "data/gfx/missile1.png", TEX_MIPMAP ) <> 0 then
                print #debug, "Texture load failure of, missile1.png"  
            end if
            
            .tech_id = i
            .graphics_tilesize = 16
            
            load_model( "data/geo/missile1.obj", .model ) 
            
            .Body = cpBodyNew( .Mass, .Mass^2 )
            cpSpaceAddBody( World.Space, .Body )
            
            .start_position = vec2f(-5000f,  (i-(max_num_of_projectiles/2)) * 100f )
            .Body->p = .start_position
            
            .shape = callocate( .model->max_triangles * sizeof( cpShape ) )
        
        for t as integer = 0 to .model->max_triangles-1
        
            for p as integer = 1 to 3
                tpoints(4-p) = .model->vertices[.model->triangles[t].point_ID(p)]
            next
            
            .shape[t] = cpPolyShapeNew( .Body, 3, tpoints(1), cpvZero )
            .shape[t]->e = 0.5
            .shape[t]->u = 0.001
            
            .shape[t]->collision_type = PROJECTILE
            
            .shape[t]->dat = cast( projectile_struct ptr, @projectiles(i) )
            
            .shape[t]->group = cast( integer, @projectiles(1) )
            cpSpaceAddShape( World.Space, .shape[t] )
            
        next
        end with
        cpResetShapeIdCounter()
    next
    
end sub



sub launch_projectile( byref plr as entity_struct, byref position as vec2f, byref direction as vec2f, byref angle as single, byref impact_force as single )
    
    for i as integer = 0 to max_num_of_projectiles - 1
        if plr.pptr[i].active = 0 and plr.pptr[i].explode =  0 then
           plr.pptr[i].active = 1
           plr.pptr[i].explosion = 0
           plr.pptr[i].explode = 0
           plr.pptr[i].impact_force = impact_force
           plr.pptr[i].body->p = position
           plr.pptr[i].body->v = direction*500f
           plr.pptr[i].body->w = 0
           plr.pptr[i].body->t = 0
           cpBodySetAngle( plr.pptr[i].body, angle )
           'print #debug, "projectile #" & i+1 & " launched!!!"
           exit sub
        end if
    next
    
end sub


sub update_projectiles( projectiles() as projectile_struct, byref loop_time as double )
    
    for i as integer = 1 to max_num_of_projectiles
        with projectiles(i)
            
            if .active then
                if .body->p.x<0 then
                    .active = 0
                    .Body->p = .start_position
                end if
                
                if .body->p.x>map_width then
                    .active = 0
                    .Body->p = .start_position
                end if
                
                if .body->p.y<0 then
                    .active = 0
                    .Body->p = .start_position
                end if
                
                if .body->p.y>map_height then
                    .active = 0
                    .Body->p = .start_position
                end if
                
            else
                if .explode<>0 then
                    .explosion+=loop_time*10
                    if .explosion>=3 then
                        .explosion = 0
                        .explode = 0
                    end if
                end if
            end if
            
        end with
    next
    
end sub


sub update_comets( comets() as projectile_struct, byref loop_time as double )
    
    for i as integer = 1 to max_num_of_comets
        with comets(i)
            
            if .active then
                if .body->p.x<0 then
                    .active = 0
                    .Body->p = .start_position
                end if
                
                if .body->p.x>map_width then
                    .active = 0
                    .Body->p = .start_position
                end if
                
                if .body->p.y<0 then
                    .active = 0
                    .Body->p = .start_position
                end if
                
                if .body->p.y>map_height then
                    .active = 0
                    .Body->p = .start_position
                end if
                
            else
                if .explode<>0 then
                    .explosion+=loop_time*10
                    if .explosion>=3 then
                        .explosion = 0
                        .explode = 0
                    end if
                end if
            end if
            
        end with
    next
    
end sub


sub load_comets( byref World as entity_struct, comets() as projectile_struct )
    dim as vec2f tpoints(1 to 3)
    
    for i as integer = 1 to max_num_of_comets
        
        with comets(i)
            .active = 1
            .mass = 1.01
            
            if Load_Texture( .sprite, "data/gfx/asteroid.png", TEX_MIPMAP ) <> 0 then
                print #debug, "Texture load failure of, asteroid.png"  
            end if
            
            .tech_id = i
            .graphics_tilesize = 64
            
            load_model( "data/geo/asteroid.obj", .model ) 
            
            .Body = cpBodyNew( .Mass, .Mass^2 )
            cpSpaceAddBody( World.Space, .Body )
            
            dim as integer mw2 = map_width\2
            dim as integer mh2 = map_height\2
            
            .start_position = vec2f(mw2-150+rnd*300,  mh2-150+rnd*300)'(i-(max_num_of_comets/2)) * 10f )
            .Body->p = .start_position
            
            .shape = callocate( .model->max_triangles * sizeof( cpShape ) )
        
        for t as integer = 0 to .model->max_triangles-1
        
            for p as integer = 1 to 3
                tpoints(4-p) = .model->vertices[.model->triangles[t].point_ID(p)]
            next
            
            .shape[t] = cpPolyShapeNew( .Body, 3, tpoints(1), cpvZero )
            .shape[t]->e = 0.5
            .shape[t]->u = 0.001
            
            .shape[t]->collision_type = COMET
            
            .shape[t]->dat = cast( projectile_struct ptr, @comets(i) )
            
            .shape[t]->group = cast( integer, @comets(i) )
            cpSpaceAddShape( World.Space, .shape[t] )
            
        next
        
        .body->v = vec2f( -10+rnd*20, -10+rnd*20 ) 
        
        end with
        cpResetShapeIdCounter()
    next
    
end sub


sub entity_control( plr() as entity_struct, byref loop_time as double )
    static as double key_time
    
    
    dim as vec2f p1 = plr(1).object[0]->get_position
    dim as vec2f p2 = plr(2).object[0]->get_position
    
    'rotate plr(1) counter-clockwise
    if multikey(FB.SC_LEFT) then
        'change object's angle (a) by turning_speed
        if abs(plr(1).object[0]->body->w)<0.03 then
            plr(1).object[0]->body->a += loop_time * plr(1).object[0]->turning_speed
            
            'nullfy angular velocity ( *not* torque)
            plr(1).object[0]->body->w = 0
        else
            if plr(1).object[0]->body->w < 0 then plr(1).object[0]->body->w+=loop_time * plr(1).object[0]->turning_speed
            if plr(1).object[0]->body->w > 0 and plr(1).object[0]->body->w < plr(1).object[0]->turning_speed then plr(1).object[0]->body->w = 0
        end if
    end if
    
    'rotate plr(1) clockwise
    if multikey(FB.SC_RIGHT) then
        if abs(plr(1).object[0]->body->w)<0.03 then
            plr(1).object[0]->body->a -= loop_time * plr(1).object[0]->turning_speed
            plr(1).object[0]->body->w = 0
        else
            if plr(1).object[0]->body->w > 0 then plr(1).object[0]->body->w-=loop_time * plr(1).object[0]->turning_speed
            if plr(1).object[0]->body->w < 0 and abs(plr(1).object[0]->body->w) < plr(1).object[0]->turning_speed then plr(1).object[0]->body->w = 0
            
        end if
    end if
          
    
    'plr(1) thrusting...
    'tell out structure to apply a thrust force in the simulation loop
    'which will move the body along it's direction vector
    if multikey(FB.SC_UP) then
    	'test
        initiateparticle( particles(), 1, 7, p1.x+cos(plr(1).object[0]->body->a)*plr(1).object[0]->StartThrustX, p1.y+sin(plr(1).object[0]->body->a)*plr(1).object[0]->StartThrustX, 0, 0, 1 )
        
        'flag the object local position
        dim as vec2f body_local = plr(1).object[0]->blp
        dim as vec2f thrust_position = vec2f( plr(1).object[0]->StartThrustX, plr(1).object[0]->StartThrustY ) 
        
        ' change ship's current speed by acceleration. if it exceeds max_velocity
        ' limit it.
        plr(1).object[0]->accel_delta += (loop_time * plr(1).object[0]->acceleration)
        if plr(1).object[0]->accel_delta > plr(1).object[0]->max_velocity then
            plr(1).object[0]->accel_delta = plr(1).object[0]->max_velocity
        end if
        
        
        dim as vec2f vel = plr(1).object[0]->Body->v
        dim as vec2f rot = plr(1).object[0]->Body->rot
        dim as single vmag = vel.magnitude
        
        'vel.normalize
        'note: **rot** already normalized through chipmunk...
        
        'dim as single abetween = pi/vel.anglebetween( rot )
        
        if vmag >= plr(1).object[0]->max_velocity then
        '    if vel.anglebetween( rot ) > vel_thold then
        '           
        '    end if
            'abetween = 0
        end if
        
        plr(1).object[0]->ePoint = body_local * plr(1).object[0]->get_rotvec * thrust_position
        plr(1).object[0]->eForce = cpvMult( plr(1).object[0]->Body->rot, plr(1).object[0]->accel_delta )
        'plr(1).object[0]->eForce = cpvMult( plr(1).object[0]->Body->rot, (plr(1).object[0]->acceleration/10) / abetween )        
    else
        plr(1).object[0]->accel_delta = 0
    end if
    
    
    'launch a missile...
    if multikey(FB.SC_RSHIFT) then
        if timer>key_time then
            key_time = timer + .25
            Launch_Projectile( plr(1), plr(1).object[0]->get_position + plr(1).object[0]->get_rotvec*140f, plr(1).object[0]->get_rotvec, plr(1).object[0]->body->a, 100 )
        end if     
    end if
    
    
    
    'rotate plr(2) counter-clockwise
    if multikey(FB.SC_A) then
        if abs(plr(2).object[0]->body->w)<0.03 then
            plr(2).object[0]->body->a += loop_time * plr(2).object[0]->turning_speed
            
            'nullfy angular velocity ( *not* torque)
            plr(2).object[0]->body->w = 0
        else
            if plr(2).object[0]->body->w < 0 then plr(2).object[0]->body->w+=loop_time * plr(1).object[0]->turning_speed
            if plr(2).object[0]->body->w > 0 and plr(2).object[0]->body->w < plr(2).object[0]->turning_speed then plr(2).object[0]->body->w = 0
        end if
    end if
    
    'rotate plr(2) clockwise
    if multikey(FB.SC_D) then
        if abs(plr(2).object[0]->body->w)<0.03 then
            plr(2).object[0]->body->a -= loop_time * plr(2).object[0]->turning_speed
            plr(2).object[0]->body->w = 0
        else
            if plr(2).object[0]->body->w > 0 then plr(2).object[0]->body->w-=loop_time * plr(2).object[0]->turning_speed
            if plr(2).object[0]->body->w < 0 and abs(plr(2).object[0]->body->w) < plr(2).object[0]->turning_speed then plr(2).object[0]->body->w = 0
        end if
    end if
    
    'plr(2) thrusting...
    'tell out structure to apply a thrust force in the simulation loop
    'which will move the body along it's direction vector
    if multikey(FB.SC_W) then
        dim as vec2f body_local = plr(2).object[0]->blp
        dim as vec2f thrust_position = vec2f( plr(2).object[0]->StartThrustX, plr(2).object[0]->StartThrustY )
        
        initiateparticle particles(), 1, 1, p2.x+cos(plr(2).object[0]->body->a)*plr(2).object[0]->StartThrustX, p2.y+sin(plr(2).object[0]->body->a)*plr(2).object[0]->StartThrustX, 0, 0, 0.6
        
        plr(2).object[0]->accel_delta += (loop_time * plr(2).object[0]->acceleration)
        
        if plr(2).object[0]->accel_delta > plr(2).object[0]->max_velocity then
            plr(2).object[0]->accel_delta = plr(2).object[0]->max_velocity
        end if
        
        plr(2).object[0]->ePoint = body_local * plr(2).object[0]->get_rotvec * thrust_position
        plr(2).object[0]->eForce = cpvMult( plr(2).object[0]->Body->rot, plr(2).object[0]->accel_delta )
    else
        plr(2).object[0]->accel_delta = 0
    end if
    
    'reset start position... debugging
    'pressing space resets both ships
    'start potition, start rotation, 0 velocity, 0 angular velocity
    if multikey(FB.SC_SPACE) then
        for i as integer = 1 to ubound(plr)
            cpBodySetAngle( plr(i).object[0]->Body, plr(i).object[0]->start_angle )
            plr(i).object[0]->set_position( plr(i).object[0]->start_position )
            cpBodyResetForces( plr(i).object[0]->Body )
            plr(i).object[0]->body->w = 0
            plr(i).object[0]->body->v = vec2f(0,0)
        next
    end if
    
end sub


sub update_entity( byref plr as entity_struct, player as integer )
    
    'loop through each entity, and each of their objects
    'resetting all external forces and the "on_ground" flag
    'if you're going to destroy an object,
    'this would be the loop to do it in
    'for p as integer = 1 to ubound(plr)
    with plr
        for o as integer = 0 to .max_objects - 1
            with *.object[o]
                
                'this means this object has been hit by a projectile
                'all that plr.pptr[] is, is a ptr to the main 
                'projectile array... just remember
                'when using [], it's 0 based
                 
                if .taken_damage <> 0 then
                        'we're going to process here, so no need to,
                        'keep the "taken_damage" flag true after this 
                        .taken_damage = 0
                        'set the projectile so that it can't hit this ship again...
                        'or anything else.
                        plr.pptr[.damage_id-1].active = 0
                        'move the physics object it waaay off the field
                        plr.pptr[.damage_id-1].Body->p = plr.pptr[.damage_id-1].start_position
                        'reset the explosion variable... this is used for animation time
                        plr.pptr[.damage_id-1].explosion = 0
                        'set the explode variable to true... this tells the, 
                        'draw projectiles sub to draw whatever sprite the explosion should use.
                        plr.pptr[.damage_id-1].explode = 1
                        
                        'calculate the position and force of the collision
                        'offset the local collision as to also rotate the body
                        'relative to the point of impact 
                        dim as single dist = plr.pptr[.damage_id-1].impact_position.distance(.get_position)
                        dim as vec2f ips =  (plr.pptr[.damage_id-1].impact_position - .get_position)
                        ips.normalize 
                        ips*=dist
                        
                        dim as vec2f vel = plr.pptr[.damage_id-1].body->v
                        vel.normalize
                        dim as vec2f frc =  vel * plr.pptr[.damage_id-1].impact_force
                        
                        cpBodyApplyForce( .body, frc, ips )
                end if
                
                '***if .collided <>0 then***                   
                'this means there was a collision with another ship detected
                'this is only here to show you how we can do this stuff non-globally
                'for instance, we add have a collided variable in our object structure now
                'it gets set to true, if the object collides with another ship
                'this happens in the ship_ship_collision_callback
                'both ships flags are set to true
                'in this callback, we can get information, such as...
                'collision point, magnitude, normal of collision, etc...
                'this is the best way to make it work properly without causing bugs...
                '***end if***
                
                .ePoint = cpv(0,0)
                .eForce = cpv(0,0)
                .eTorque = 0
                .on_ground = 0
                
                if .Body->p.x < 0 then
                    .Body->p.x = map_width
                end if
                
                if .Body->p.x > map_width then
                    .Body->p.x = 0
                end if
                
                if .Body->p.y < 0 then
                    .Body->p.y = map_height
                end if
                
                if .Body->p.y > map_height then
                    .Body->p.y = 0
                end if
                
                dim as vec2f vel =.Body->v 
                dim as single vmag = vel.magnitude
                
                if (player = 1 and MultiKey(FB.SC_UP)) or (player = 2 and MultiKey(FB.SC_W)) then
                if vmag > .max_velocity then
                    vel.normalize
                    .Body->v.x = vel.x*.max_velocity
                    .Body->v.y = vel.y*.max_velocity
                    .Body->i = 0
                    'print #debug, vmag
                end if
                end if
                
                if abs(.Body->w>1f) then
                    .Body->w = 1f * sgn(.Body->w)
                    'print #debug, "rotation damped!"
                end if
                
                .tmatrix.LoadIdentity()
                if .is_world <> 0 then
                    .tmatrix.translate( .start_position )
                elseif .is_world = 0 then
                    .tmatrix.translate( .get_position )
                end if
                .tmatrix.rotate( 0f, 0f, .get_angle * inv_pi_180 )
                
            end with
        next
        
        
    end with
    'next
    
end sub


sub update_simulation( byref fSteps as integer, byref dt as single, byref World as entity_Struct, plr() as entity_struct )
    
    for i as integer = 1 to fSteps
        
        cpSpaceStep( World.Space, dt )
        
        for p as integer = 1 to ubound(plr)
            with plr(p)
                for o as integer = 0 to .max_objects-1
                    with *.object[o]
                        cpBodyResetForces( .Body )
                        .Body->t+=.etorque
                        cpBodyApplyforce( .Body, .eForce, .ePoint )
                        
                        
                        for w as integer = 0 to world.max_objects - 1
                            dim as vec2f bpos = .get_position
                            dim as vec2f wpos = world.object[w]->tmatrix.position
                            dim as single dist = bpos.distance(wpos)
                            if dist>0 then
                                dim as single gforce = GRAVITY * ((.mass * world.object[w]->mass) / dist ^ 2f)
                                dim as vec2f tgrav = ( (wpos-bpos) / dist) * (gforce/.mass)
                                cpBodyApplyforce( .Body, tgrav, .blp )
                            end if
                        next
                    end with
                next
            end with
        next
        
    next
    
    
    
end sub


function Load_Texture( byref Texture as Gluint, byref filename as string, byref filter as integer ) as integer
    
    dim SprWidth as integer
    dim SprHeight as integer
    png_dimensions( filename, SprWidth, SprHeight )
    
    dim as fb.image ptr buffer = png_load( filename, PNG_TARGET_OPENGL )
    
    if buffer = 0 then
        return 1
    end if
    
    glGenTextures( 1, @Texture )
    glBindTexture( GL_TEXTURE_2D, Texture )
    Glenable( GL_TEXTURE_2D )
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, SprWidth, SprHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer )
    
    if filter = TEX_MIPMAP then
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT )
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT )
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR )
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR )
        gluBuild2DMipmaps( Gl_Texture_2D, GL_RGBA, SprWidth, SprHeight, GL_RGBA, GL_UNSIGNED_BYTE, buffer )
    elseif filter = TEX_NEAREST then
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT )
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT )
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST )
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST )
    end if
    
    glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE )
    
    imagedestroy(buffer)
    
    return 0
end function


function Get_Sprite_From_Texture( byref source as gluint, byref start_x as integer, byref start_y as integer, byref get_width as integer, byref get_height as integer, byref src_width as integer, byref src_height as integer ) as gluint
    dim as gluint temptex
    
    dim as glubyte ptr pdata = new glubyte[ (get_width+1) * (get_height+1) * 4 ]
    
    set_ortho( display.w, display.h )
    glDisable( GL_DEPTH_TEST )
    
    
    glClear( GL_COLOR_BUFFER_BIT )
    
    glEnable( GL_TEXTURE_2D )
    glBindTexture( GL_TEXTURE_2D, source )
    
    glBegin( GL_QUADS )
    glTexCoord2D( 0, 1 )
    GlVertex2i( 0, src_height )
    
    glTexCoord2D( 0, 0 )
    GlVertex2i( 0, 0 )
    
    glTexCoord2D( 1, 0 )
    GlVertex2i( src_width, 0 )
    
    glTexCoord2D( 1, 1 )
    glVertex2i( src_width, src_height )
    glEnd()
    
    glReadPixels( start_x, start_y, get_width, get_height, GL_RGBA, GL_UNSIGNED_BYTE, pdata )
    
    glGenTextures( 1, @temptex )
    glBindTexture( GL_TEXTURE_2D, temptex )
    glEnable( GL_TEXTURE_2D )
    
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, get_width, get_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pdata )
    gluBuild2DMipmaps( GL_TEXTURE_2D, GL_RGBA, get_width, get_height, GL_RGBA, GL_UNSIGNED_BYTE, pdata )
    
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE )
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE )
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR )
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR )
    glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE )
    
    dim as gluint dList = GlGenLists(1)
    glNewList( dList, GL_COMPILE )
    
    glEnable( GL_TEXTURE_2D )
    glBindTexture( GL_TEXTURE_2D, temptex )
    
    
    glBegin( GL_QUADS )
    glTexCoord2D( 0, 1 )
    glVertex2f( -get_width/2, get_height/2 )
    
    glTexCoord2D( 0, 0 )
    glVertex2f( -get_width/2, -get_height/2 )
    
    glTexCoord2D( 1, 0 )
    glVertex2f( get_width/2, -get_height/2 )
    
    glTexCoord2D( 1, 1 )
    glVertex2f( get_width/2, get_height/2 )
    glEnd()
    glEndList()
    
    drop_ortho()
    
    delete[] pdata
    'glDeleTetextures( 1, @temptex )
    
    return dList
end function


function init_physics_world( byref world as entity_struct ) as integer
    
    cpInitChipmunk()
    cpResetShapeIdCounter()
    
    world.Space = cpSpaceNew()
    
    if world.Space<> 0 then
        'w->damping = .75
        world.Space->iterations = 10
        'w->gravity = vec2f(0, -9.8*10f)
        
        cpSpaceResizeStaticHash( world.Space, 64.0, 100)
        cpSpaceResizeActiveHash( world.Space, 32.0, 10000 )
        return 0
    else 
        return 1
    end if
    
end function


sub entity_add_object( byref world as entity_struct, plr as entity_struct, byref script_file as string, byref is_world as integer )
    
    dim row as string
    dim as integer filenum = freefile
    
    'i'm creating this matrix as a temporary fix to set random positions
    'it will never set them inside the planet, but they may hit each other
    'we can fix that, if needed.
    dim as matrix tmat
    tmat.LoadIdentity()
    tmat.Rotate( 0f,0f, rnd*360f )
    dim as single sdisx = map_width/2 -800 +rnd*500
    dim as single sdisy = map_height/2 -800 +rnd*500
    var spos = vec2f( sdisx, sdisy ) '* tmat
    
    'this sets the angle of each ship to point directly away from the planet
    'we can randomize this, if needed... or whatever. ;) 
    dim as single sang = atan2( spos.y, spos.x )
    
    
    'temporary array used for storing vertices(points)
    dim as vec2f tpoints(1 to 3)
    
    'i'm using "with" because i hate typing plr.blah_blah_blah
    'it's also faster
    'what this is doing, is telling the ship entity that we want to add another object to it
    'for instance, if we wanted to have a ship with several parts,
    'we would add eaCh new object using this method.
    'so... say we have a main hull object... that would be plr.object[0]
    '(it should always be loaded first because that will be the one we use to control the ship
    'look in the entity_control sub for more details...)
    'then we want to add another piece... maybe a wing
    'we use this method, and instead of the model file being "ship1_main_hull.obj",
    'it would be like, "ship1_left_wing.obj"
    'this method will automatically load in into plr.object[1],    
    'of the ship's object array
    'add the right wing... automaticaly goes into plr.object[2]
    'so on, so forth... 
    'that's all this really is, in essence.
    'since we don't have dynamic arrays in types, i'm forcing it
    'and that's why plr.max_objects increases each time
    with plr
        .max_objects+=1
        .object = reallocate( .object, .max_objects * sizeof( object_struct ) )
        .object[ .max_objects-1 ] =  callocate( sizeof( object_struct) )
    end with
    
    'load the scripting...
    'in my *humble opinion* we should be using a "with...end with" block here.
    'i've taken the liberty of adding "plr.object[plr.max_objects - 1]" to every single one of these entries though,
    'to keep yer bitch ass from whining in my ear :p
    'also, i've used a block to do the first couple... just so you could see the benefit.
    'the benefit... we have a double pointer.
    'very slow to dereference...
    '"with *plr.object[plr.max_objects - 1]" dereferences that "ONE TIME ONLY!"
    'major readability increase for me... and WAAAAY faster.
    'also, it allows you to use the ".whatever" again because it's already a dereferenced pointer.
    
    with *plr.object[plr.max_objects - 1] 
        open script_file for input as #filenum
        
     	input #filenum, row
        .ObjName = ltrim(rtrim(ParseScriptLine(row)))
        input #filenum, row
        .acceleration = val(ParseScriptLine(row))
        input #filenum, row
        .max_velocity = val(ParseScriptLine(row))
        input #filenum, row
        .turning_speed = val(ParseScriptLine(row))
        input #filenum, row
        .WeaponPower = val(ParseScriptLine(row))
        input #filenum, row
        .WeaponFireRatio = val(ParseScriptLine(row))
        input #filenum, row
        .Second_WeaponPower = val(ParseScriptLine(row))
        input #filenum, row
        .Hull = val(ParseScriptLine(row))
        input #filenum, row
        .Energy = val(ParseScriptLine(row))
        input #filenum, row
        .EnergyRecharge = val(ParseScriptLine(row))
        input #filenum, row
        .StartWeaponX = val(ParseScriptLine(row))
        input #filenum, row
        .StartWeaponY = val(ParseScriptLine(row))
        input #filenum, row
        .StartThrustX = val(ParseScriptLine(row))
        input #filenum, row
        .StartThrustY = val(ParseScriptLine(row))
        input #filenum, row
        .Graphics_File = ltrim(rtrim(ParseScriptLine(row)))
        input #filenum, row
        .Model_File = ltrim(rtrim(ParseScriptLine(row)))
        input #filenum, row
        .Graphics_TileSize = val(ParseScriptLine(row))
        input #filenum, row
        .Mass = val(ParseScriptLine(row))
        input #filenum, row
        .LightRadius = val(ParseScriptLine(row))
        if is_world <> 0 then
            input #filenum, row
            .start_position.x = val(ParseScriptLine(row))
            input #filenum, row
            .start_position.y = val(ParseScriptLine(row))
        end if
        close #filenum
    end with
    
    'all this section is doing, is using the texture name frm the script file,
    'to load the texture into the object's sprite variable, which is now a gluint ;)
    if Load_Texture( plr.object[plr.max_objects - 1]->sprite, "data/gfx/" & plr.object[plr.max_objects - 1]->Graphics_File, TEX_MIPMAP ) <> 0 then
        'beep
        print #debug, "Texture load failure of, " & "data/gfx/" & plr.object[plr.max_objects - 1]->Graphics_File 
    end if
    
    'again, using "with" for ease and speed, while sacrificing a bit of readability
    'you shold get used to seeing this after some time and keep in mind that you're in a
    '"with...end with" block, so everything pertains to the current "with whatever"
    with *plr.object[plr.max_objects - 1]
        .is_world = is_world
        .turn_damp = .995
        'each object has a model file embedded within,
        'you already know this is used for the physics simulation
        'so, we're just using the data from the object's script file,
        'to load the actual geometry into the model pointer
        'we're using "new" to grab some memory for it,
        'and then actually loading it 
        '.model = callocate( sizeof(model_struct) )
        load_model( "data/geo/" & .Model_File, .model )
        
        'ok, here we're telling shipmunk to create a new body container
        'this will be the master for this particular object in the physics world
        'Mass is the mass of the object, obviously,
        'and the second param, "Mass*200" is the amount of torque it requires,
        'to rotate the object
        'this param will eventually be loaded from the script file
        
        
        if is_world <> 0 then
            .Body = cpBodyNew( 1e31, 1e31 )
        elseif is_world = 0 then
            .Body = cpBodyNew( .Mass, .Mass*200f )
            cpSpaceAddBody( World.Space, .Body )
        end if
        
        'set the starting position and starting angle for the object
        'remeber, these are random... i used previous matrix for this
        if is_world = 0 then
            .start_position = spos
            .set_position( .start_position )
            cpBodySetAngle( .Body, sang )
            .start_angle = sang
        end if
        
        .shape = callocate( .model->max_triangles * sizeof( cpShape ) )
        'loop through the model's entire triangle list
        for t as integer = 0 to .model->max_triangles-1
            
            'now loop through each vertex(point) of the triangle in reverse,
            'and add it to the temp array
            'the reason we're gong in reverse is because lightwve uses a different method
            'than chipmunk does... this makes it compatible
            for p as integer = 1 to 3
                if is_world <> 0 then
                    tpoints(4-p) = .model->vertices[.model->triangles[t].point_ID(p)] + .start_position
                elseif is_world = 0 then
                    tpoints(4-p) = .model->vertices[.model->triangles[t].point_ID(p)]
                end if    
                'print #debug, tpoints(p).x
            next
            
            'now, with the vertex(point) array we just created,
            'we're gong to tel chipmunk to create a new shape using it
            'for our stuff, these will always be triangles
            'this is what chipmunk will actualy use for the collision detection geometry,
            'similar to geo_sprite_collide, on a full blown physics implementation
            .shape[t] = cpPolyShapeNew( .Body, 3, tpoints(1), cpvZero )
            
            'set some properties for the actual shape here
            'shape->e = elasticity = how much does it bounce?
            'shape->u = friction = how rough is the surface?
            .shape[t]->e = 0.5
            .shape[t]->u = 0.001
            
            'group is an unique collision identifier
            'we're sending the ptr to the plr entity
            'this makes an unique collision id that chipmunk will use internally
            'also, we can use this id when objects collide with each other
            'we'll get into this more later...
            '.shape[t]->group = cast( integer, @plr )
            
            'this is the collision type
            'basically, it will only be used in the collision callbacks
            'this means that chipmunk will always know that this is a ship,
            'and will always know that the terrain is the terrain
            if is_world <> 0 then
                .shape[t]->collision_type = TERRAIN
            elseif is_world = 0 then
                .shape[t]->collision_type = SHIP
            end if                
            'here, we're giving a ptr to the object to chipmunk, for this exact shape
            'this means that in our callback, we will be able to grab this exact object,
            'by casting a ptr back to a temporary object
            'in otherwords... when a ship hits another ship, or a ship hits the planet,
            'we'll have this ptr to work with, so we can do things like,
            'cause damage to the ship for hitting the planet, or,
            'cause variable damage to both ships for hitting each other. ;) 
            .shape[t]->dat = cast( object_struct ptr, plr.object[plr.max_objects - 1] )
            
            'finally, we add the shape to the physics world so newton knows what to do with it.
            'you must alway remember to do this, or else objects will just fly through eact other,
            'with no consequence.
            'cpSpaceAddShape( World.Space, .shape[t] )
            
            if is_world <> 0 then
                .shape[t]->group = t
                cpSpaceAddStaticShape( World.Space, .shape[t] )
                cpSpaceRehashStatic(World.Space)
            elseif is_world = 0 then
                .shape[t]->group = cast( integer, @plr )
                cpSpaceAddShape( World.Space, .shape[t] )
            end if
            
            
            
        next
        
    end with
    
    
    cpResetShapeIdCounter()
    
end sub


sub destroy_entity( byref world as entity_struct, byref plr as entity_struct )
    
    
    for j as integer = 0 to plr.max_joints - 1
        cpSpaceRemoveJoint( world.space, plr.joint[j]->joint )
        deallocate( plr.joint[j] )
        plr.joint[j] = 0
    next
    
    if plr.joint then
        deallocate( plr.joint )
        plr.joint = 0
        plr.max_joints = 0
    end if
    
    
    for o as integer = 0 to plr.max_objects-1
        
        glDeleteTextures( 1, @plr.object[o]->sprite )
        
        for t as integer = 0 to plr.object[o]->model->max_triangles-1
            if plr.object[o]->is_world = 0 then
                cpSpaceRemoveShape( world.space, plr.object[o]->shape[t] )
            end if
        next
        
        deallocate( plr.object[o]->shape )
        plr.object[o]->shape = 0
        
        cpSpaceRemoveBody( world.space, plr.object[o]->body )
        
        unload_model( plr.object[o]->model )
        
        deallocate( plr.object[o] )
        plr.object[o] = 0
        
    next
    
    deallocate( plr.object )
    plr.object = 0
    plr.max_objects  = 0
    
    cpResetShapeIdCounter()
    
end sub


function ParseScriptLine (script_file_line as string) as string               
    
    dim as ubyte state=0                                             
    dim as string c                                                  
    
    dim as string varval, varname                                                                                                                  
    
    for i as integer = 1 to len(script_file_line)                    
        c = mid(script_file_line,i,1)                                
        if c = "=" and state = 0 then state += 1                     
        if c <> " " then                                             
            if state = 0 then varname += c                           
            if state > 0 and c <> "=" then varval += c               
        end if                                                       
    next      
    
    ParseScriptLine = varval
    
    print #debug, varval
end function

sub DrawShips (plr() as entity_struct, cmat as matrix)
    
    dim as integer xadd, yadd    
    dim as matrix ship_mat(2)
    dim as vec2f p1 = plr(1).object[0]->get_position
    dim as vec2f p2 = plr(2).object[0]->get_position
    
    ship_mat(1).LoadIdentity()
    ship_mat(2).LoadIdentity()
    
    ' pass the camera matrix to ship matrix.
    ship_mat(1) = cmat
    ship_mat(2) = cmat
    
    ' translate the matrix for mainship x and y.
    ship_mat(1).Translate( csng(p1.X), csng(p1.Y), 1f )
    'ship1_mat.Rotate(0, 0, MainShip(1).AngleDeg )
    ship_mat(2).Translate( csng(p2.X), csng(p2.Y), 1f )
    
    'glEnable( GL_BLEND )
    'glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
    
    glColor4F( 1,1,1,1 )
    
	'your sprite thing... however you were doing that
    'hasn't beenn added in yet
    'i figured you could do that though, since you already have it going
    'in the old one
    'draw the ships
    glEnable( GL_BLEND )
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
    glColor3f( 1,1,1 )
    for p as integer = 1 to ubound(plr)
        with plr(p)
            for o as integer = 0 to .max_objects - 1
                with *.object[o]
                    rotozoomgl_imm( .sprite, ship_mat(p).Position.X, ship_mat(p).Position.Y, .get_angle * inv_pi_180, zoom, .Graphics_TileSize, .Graphics_TileSize, .LightRadius )    
                    
                    for go_around_playfield as integer = 1 to 8
                        
                        select case as const go_around_playfield
                            case 1
                                xadd = -map_width
                                yadd = -map_height
                            case 2
                                xadd = 0
                                yadd = -map_height
                            case 3
                                xadd = map_width
                                yadd = -map_height
                            case 4
                                xadd = map_width
                                yadd = 0
                            case 5
                                xadd = map_width
                                yadd = map_height
                            case 6
                                xadd = 0
                                yadd = map_height
                            case 7
                                xadd = -map_width
                                yadd = map_height
                            case 8
                                xadd = -map_width
                                yadd = 0
                        end select
                        
                        rotozoomgl_imm( .sprite, ship_mat(p).Position.X+xadd*zoom, ship_mat(p).Position.Y+yadd*zoom, .get_angle * inv_pi_180, zoom, .Graphics_TileSize, .Graphics_TileSize, .LightRadius )    
                        
                      
                    
                    
                    'If draw_debug_lines then
'                                         glDisable( GL_TEXTURE_2D )
'                                         glColor3f(1,1,1)
'                                                
'                                         glPushMatrix()
'                                         glTranslatef( .tmatrix.position.x, .tmatrix.position.y, 0 )
'                                         glRotateF( .get_angle * inv_pi_180, 0,0, 1 )
'                                         glScalef( zoom, zoom, 1 )
'                                                
'                                         glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )
'                                         glBegin( GL_TRIANGLES )
'                                         For t as integer = 0 to .model->max_triangles - 1
'                                           For p as integer = 1 to 3
'                                               glVertex3f( .model->vertices[ .model->triangles[t].Point_ID(p)].x, .model->vertices[ .model->triangles[t].Point_ID(p)].y, 0 )
'                                           next
'                                         Next
'                                           glEnd()
'                                           glPopMatrix()
'                                           glEnable( GL_TEXTURE_2D )
'                                           glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )
                    'End if
                    next go_around_playfield 
                end with
            next
        end with
    next
    
end sub





sub DrawPlanets ( world as entity_struct, plr() as entity_struct )
	
    dim as integer xadd, yadd, xdistance, ydistance
    dim as matrix tmat
    
    
    dim as vec2f p1 = plr(1).object[0]->get_position
    dim as vec2f p2 = plr(2).object[0]->get_position
    
    XDistance = p1.X-p2.X
    YDistance = p1.Y-p2.Y
    
    if p1.X < map_width/2 and p2.X > map_width / 2 and abs(p1.X-p2.X) > abs(p1.X-(p2.X - map_width)) then XDistance = p1.X-(p2.X-map_width)
    if p1.X > map_width/2 and p2.X < map_width / 2 and abs(p1.X-p2.X) > abs((p1.X-map_width) - p2.X) then XDistance = (p1.X-map_width)-p2.X
    if p1.Y < map_height/2 and p2.Y > map_height / 2 and abs(p1.Y-p2.Y) > abs(p1.Y-(p2.Y - map_height)) then YDistance = p1.Y-(p2.Y-map_height)
    if p1.Y > map_height/2 and p2.Y < map_height / 2 and abs(p1.Y-p2.Y) > abs((p1.Y-map_height)- p2.Y) then YDistance = (p1.Y-map_height)-p2.Y
    
    Camera_CenterX = -(p1.X - ScreenXMid/zoom - (XDistance)/2)
    Camera_CenterY = -(p1.Y - ScreenYMid/zoom - (YDistance)/2)
    
    'glEnable( GL_BLEND )
    'glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
    
    glColor4f( 1,1,1,1 )
    
    
    for i as integer = 0 to world.max_objects - 1    
        dim as integer planetx = world.object[i]->tmatrix.position.x, planety = world.object[i]->tmatrix.position.y
        tmat.LoadIdentity()
        tmat.Scale( zoom, zoom, 1f )
        tmat.Translate( Camera_CenterX, Camera_CenterY, 1f )
        tmat.Translate( csng(planetx), csng(planety), 0  )
        
        'draw the planet
        rotozoomgl_imm( world.object[i]->sprite, tmat.Position.X, tmat.Position.Y, 0, zoom, world.object[i]->Graphics_TileSize, world.object[i]->Graphics_TileSize, 2)
        
        for go_around_playfield as integer = 1 to 8
            
            select case as const go_around_playfield
                case 1
                    xadd = -map_width
                    yadd = -map_height
                case 2
                    xadd = 0
                    yadd = -map_height
                case 3
                    xadd = map_width
                    yadd = -map_height
                case 4
                    xadd = map_width
                    yadd = 0
                case 5
                    xadd = map_width
                    yadd = map_height
                case 6
                    xadd = 0
                    yadd = map_height
                case 7
                    xadd = -map_width
                    yadd = map_height
                case 8
                    xadd = -map_width
                    yadd = 0
            end select
            
            rotozoomgl_imm( world.object[i]->sprite, tmat.Position.X+xadd*zoom, tmat.Position.Y+yadd*zoom, 0, zoom, world.object[i]->Graphics_TileSize, world.object[i]->Graphics_TileSize, 2)
            
        next go_around_playfield
    next
    
end sub


sub DrawNebula ( nebula() as sprite_struct, plr() as entity_struct, Layer_Speed as single, Layer_Transparency as single)
    
    dim as integer xadd, yadd, xdistance, ydistance
    dim as matrix tmat
    dim as integer camera_centerx, camera_centery
    dim as vec2f p1 = plr(1).object[0]->get_position
    dim as vec2f p2 = plr(2).object[0]->get_position
    
    if Layer_Speed = 0 then Layer_Speed = 1
    
    XDistance = p1.X-p2.X
    YDistance = p1.Y-p2.Y
    
    if p1.X < map_width/2 and p2.X > map_width / 2 and abs(p1.X-p2.X) > abs(p1.X-(p2.X - map_width)) then XDistance = p1.X-(p2.X-map_width)
    if p1.X > map_width/2 and p2.X < map_width / 2 and abs(p1.X-p2.X) > abs((p1.X-map_width) - p2.X) then XDistance = (p1.X-map_width)-p2.X
    if p1.Y < map_height/2 and p2.Y > map_height / 2 and abs(p1.Y-p2.Y) > abs(p1.Y-(p2.Y - map_height)) then YDistance = p1.Y-(p2.Y-map_height)
    if p1.Y > map_height/2 and p2.Y < map_height / 2 and abs(p1.Y-p2.Y) > abs((p1.Y-map_height)- p2.Y) then YDistance = (p1.Y-map_height)-p2.Y
    
    Camera_CenterX = -(p1.X - ScreenXMid/zoom*Layer_Speed - (XDistance)/2)
    Camera_CenterY = -(p1.Y - ScreenYMid/zoom*Layer_Speed - (YDistance)/2)
    
    glColor4F( 1,1,1, Layer_Transparency)
    
    for count_nebula as integer = 1 to num_of_max_nebulas
        
        'Dim as vec2f neb_pos = nebula(count_nebula).object[0]->get_position
        tmat.LoadIdentity()
        tmat.Scale( zoom/Layer_Speed, zoom/Layer_Speed, 1f )
        tmat.Translate( Camera_CenterX, Camera_CenterY, 1f )
        tmat.Translate( nebula(count_nebula).position.x, nebula(count_nebula).position.y, 0  )
        
        rotozoomgl( nebula(count_nebula).sprite, tmat.Position.X, tmat.Position.Y, 0, zoom*5)
        
        for go_around_playfield as integer = 1 to 8
            
            select case as const go_around_playfield
                case 1
                    xadd = -map_width/Layer_Speed
                    yadd = -map_height/Layer_Speed
                case 2
                    xadd = 0
                    yadd = -map_height/Layer_Speed
                case 3
                    xadd = map_width/Layer_Speed
                    yadd = -map_height/Layer_Speed
                case 4
                    xadd = map_width/Layer_Speed
                    yadd = 0
                case 5
                    xadd = map_width/Layer_Speed
                    yadd = map_height/Layer_Speed
                case 6
                    xadd = 0
                    yadd = map_height/Layer_Speed
                case 7
                    xadd = -map_width/Layer_Speed
                    yadd = map_height/Layer_Speed
                case 8
                    xadd = -map_width/Layer_Speed
                    yadd = 0
            end select
            
            rotozoomgl( nebula(count_nebula).sprite, tmat.Position.X+xadd*zoom, tmat.Position.Y+yadd*zoom, 0, zoom*5)
            
        next go_around_playfield
        
    next count_nebula
    
    
end sub

sub DrawStars ( star() as sprite_struct,  plr() as entity_struct, starset as integer, layer_speed as single, layer_transparency as single )
    
    dim as integer xadd, yadd, xdistance, ydistance
    dim as matrix tmat
    dim as integer camera_centerx, camera_centery
    dim as vec2f p1 = plr(1).object[0]->get_position
    dim as vec2f p2 = plr(2).object[0]->get_position
    
    if Layer_Speed = 0 then Layer_Speed = 1
    
    XDistance = p1.X-p2.X
    YDistance = p1.Y-p2.Y
    
    if p1.X < map_width/2 and p2.X > map_width / 2 and abs(p1.X-p2.X) > abs(p1.X-(p2.X - map_width)) then XDistance = p1.X-(p2.X-map_width)
    if p1.X > map_width/2 and p2.X < map_width / 2 and abs(p1.X-p2.X) > abs((p1.X-map_width) - p2.X) then XDistance = (p1.X-map_width)-p2.X
    if p1.Y < map_height/2 and p2.Y > map_height / 2 and abs(p1.Y-p2.Y) > abs(p1.Y-(p2.Y - map_height)) then YDistance = p1.Y-(p2.Y-map_height)
    if p1.Y > map_height/2 and p2.Y < map_height / 2 and abs(p1.Y-p2.Y) > abs((p1.Y-map_height)- p2.Y) then YDistance = (p1.Y-map_height)-p2.Y
    
    ' camera pos needs to be recalculated taking into consideration layer_speed (distance)		
    Camera_CenterX = -(p1.X - ScreenXMid/zoom*Layer_Speed - (XDistance)/2)
    Camera_CenterY = -(p1.Y - ScreenYMid/zoom*Layer_Speed - (YDistance)/2)
    
    ' Loop through stars
    for countstar as integer = 1 to num_of_max_stars
        
        ' Disable texturing for PSET (dot) starts, enable
        ' it back with big stars (at the end of stars array).
        if countstar = 1 then glDisable( GL_TEXTURE_2D )
        if countstar = num_of_max_stars - num_of_big_stars and starset < 3 then 
            glEnable( GL_TEXTURE_2D )
            ' Set layer transparency level
            glColor4F( 1,1,1, layer_transparency )
        end if
        
        ' Load the temp matrix and move/scale it according
        ' to camera position and zoom.
        tmat.LoadIdentity()
        tmat.Scale( zoom/layer_speed, zoom/layer_speed, 1f )
        tmat.Translate( Camera_CenterX, Camera_CenterY, 1f )
        tmat.Translate( csng(Star(starset,countstar).position.X), csng(Star(starset,countstar).position.Y), 0 )
        
        ' According to star type draw a dot or paste star sprite.
        if Star(starset,countstar).Typ < 4 then psetgl2 tmat.Position.X, tmat.Position.Y, Star(starset,countstar).MColor
        if Star(starset,countstar).Typ > 3 then rotozoomgl( star_tex(Star(starset,countstar).Typ-3), tmat.Position.X,tmat.Position.Y, 0, 1)
        
        ' Loop through all 8 edge sectors around the map to create
        ' an illusion of endless playfield.
        for go_around_playfield as integer = 1 to 8
            
            select case as const go_around_playfield
                case 1
                    xadd = -map_width/layer_speed
                    yadd = -map_height/layer_speed
                case 2
                    xadd = 0
                    yadd = -map_height/layer_speed
                case 3
                    xadd = map_width/layer_speed
                    yadd = -map_height/layer_speed
                case 4
                    xadd = map_width/layer_speed
                    yadd = 0
                case 5
                    xadd = map_width/layer_speed
                    yadd = map_height/layer_speed
                case 6
                    xadd = 0
                    yadd = map_height/layer_speed
                case 7
                    xadd = -map_width/layer_speed
                    yadd = map_height/layer_speed
                case 8
                    xadd = -map_width/layer_speed
                    yadd = 0
            end select
            
            ' Draw stars in these sectors if they are inside the visible
            ' screen.
            if Star(starset,countstar).Typ < 4 and tmat.Position.X + xadd*zoom > 0 and tmat.Position.X + xadd*zoom < ScreenWidth and tmat.Position.Y + yadd*zoom > 0 and tmat.Position.Y + yadd*zoom < ScreenHeight then psetgl2 tmat.Position.X+xadd*zoom, tmat.Position.Y+yadd*zoom, Star(starset,countstar).MColor
            if Star(starset,countstar).Typ > 3 then
                if tmat.Position.X + xadd*zoom > 0 and tmat.Position.X + xadd*zoom < ScreenWidth and tmat.Position.Y + yadd*zoom > 0 and tmat.Position.Y + yadd*zoom  < ScreenHeight then rotozoomgl( star_tex(Star(starset,countstar).Typ-3), tmat.Position.X+xadd*zoom,tmat.Position.Y+yadd*zoom, 0, 1)
            end if  
            
        next go_around_playfield
        
    next countstar
    
end sub


sub ResetParticle( particle() as particle_struct, byval num as integer, byval clr as integer, byval xDir as single, byval yDir as single, byval plife as single)
    
    if clr > 11 then clr = 11
    '' Make the particels active
    particle(num).active = TRUE
    '' Give the particle life
    particle(num).life = plife 
    '' Random Fade Speed
    particle(num).fade = 0.01
    '' Select Red Rainbow Color
    particle(num).r = colors_table(clr, 0)
    '' Select Green Rainbow Color
    particle(num).g = colors_table(clr, 1)
    '' Select Blue Rainbow Color
    particle(num).b = colors_table(clr, 2)
    '' Set the position on the X axis
    'particle(num).x = 0.0f
    '' Set the position on the Y axis
    'particle(num).y = 0.0f
    '' Set the position on the Z axis
    particle(num).z = 0.0f
    '' Random Speed On X Axis
    particle(num).xi = xDir
    '' Random Speed On Y Axi
    particle(num).yi = yDir
    
end sub

sub InitiateParticle ( particles() as particle_struct, byval typ as integer, byval clr as integer, byval xpos as double, byval ypos as double, byval xDir as single, byval yDir as single, byval plife as single )
    
    'dim as integer tindex = 0
    
    ' initiate a particle if a free one is found
    'for i AS INTEGER = count_start_particle to MAX_PARTICLES-1
    'if particles(count_start_particle).active = FALSE THEN
    
    particles(count_start_particle).x = xpos
    particles(count_start_particle).y = ypos
    particles(count_start_particle).typ = typ
    
    ResetParticle (particles(), count_start_particle, clr, xDir, yDir, plife)
    count_start_particle = count_start_particle + 1
    if count_start_particle > max_num_of_particles then count_start_particle = 1 
    'IF count_start_particle = 300 THEN
    'Open "fade_part_report.txt" For Output As #2
    'FOR countpart AS INTEGER = 0 TO count_start_particle
    'IF particles(countpart).active = TRUE THEN Print #2, particles(countpart).life*1000
    'NEXT countpart
    'Close #2
    'END IF
    
end sub

sub DrawParticles (particles() as particle_struct, plr() as entity_struct, byref loop_time as double)
    
    dim as integer xadd, yadd, xdistance, ydistance
    dim as matrix tmat
    
    dim as vec2f p1 = plr(1).object[0]->get_position
    dim as vec2f p2 = plr(2).object[0]->get_position
    
    XDistance = p1.X-p2.X
    YDistance = p1.Y-p2.Y
    
    if p1.X < map_width/2 and p2.X > map_width / 2 and abs(p1.X-p2.X) > abs(p1.X-(p2.X - map_width)) then XDistance = p1.X-(p2.X-map_width)
    if p1.X > map_width/2 and p2.X < map_width / 2 and abs(p1.X-p2.X) > abs((p1.X-map_width) - p2.X) then XDistance = (p1.X-map_width)-p2.X
    if p1.Y < map_height/2 and p2.Y > map_height / 2 and abs(p1.Y-p2.Y) > abs(p1.Y-(p2.Y - map_height)) then YDistance = p1.Y-(p2.Y-map_height)
    if p1.Y > map_height/2 and p2.Y < map_height / 2 and abs(p1.Y-p2.Y) > abs((p1.Y-map_height)- p2.Y) then YDistance = (p1.Y-map_height)-p2.Y
    
    Camera_CenterX = -(p1.X - ScreenXMid/zoom - (XDistance)/2)
    Camera_CenterY = -(p1.Y - ScreenYMid/zoom - (YDistance)/2)
    
    glEnable( GL_BLEND )
    glBlendFunc( GL_SRC_ALPHA, GL_ONE )
    
    glColor4f( 1,1,1,1 )
    
    for i as integer = 1 to max_num_of_particles
        
        if ( particles(i).active = TRUE ) then
            
            ' translate the particle    
            tmat.LoadIdentity()
            tmat.Scale( zoom, zoom, 1f )
            tmat.Translate( Camera_CenterX, Camera_CenterY, 1f )
            tmat.Translate( csng(particles(i).x), csng(particles(i).y), 0  )
            
            '' Grab Our Particle X Position
            dim as single x = tmat.Position.X ' particles(i).x
            '' Grab Our Particle Y Position
            dim as single y = tmat.Position.y ' particles(i).y
            
            '' Draw The Particle Using Our RGB Values,
            '' Fade The Particle Based On It's Life
            glColor4F( particles(i).r,particles(i).g,particles(i).b, particles(i).life) 
            
            '' Move On The X Axis By X Speed
            particles(i).x += particles(i).xi*loop_time*20 '/ ( slowdown * 1000 )
            '' Move On The Y Axis By Y Speed
            particles(i).y += particles(i).yi*loop_time*20 '/ ( slowdown * 1000 )
            
            '' Reduce Particles Life By 'Fade'
            particles(i).life = particles(i).life - particles(i).fade*loop_time*100
            
            '' If the particle dies, diactivate it
            if ( particles(i).life < 0.0f ) then
                particles(i).active = FALSE
            end if 
            
            if particles(i).typ = 1 then rotozoomgl( ParticleTex, x, y, 0, zoom/1.6)
            if particles(i).typ = 2 then rotozoomgl( ParticleTex, x, y, 0, zoom/1.4)
            if particles(i).typ = 3 then rotozoomgl( ParticleTex, x, y, 0, zoom/3)
            
            for go_around_playfield as integer = 1 to 8
                
                select case as const go_around_playfield
                    case 1
                        xadd = -map_width
                        yadd = -map_height
                    case 2
                        xadd = 0
                        yadd = -map_height
                    case 3
                        xadd = map_width
                        yadd = -map_height
                    case 4
                        xadd = map_width
                        yadd = 0
                    case 5
                        xadd = map_width
                        yadd = map_height
                    case 6
                        xadd = 0
                        yadd = map_height
                    case 7
                        xadd = -map_width
                        yadd = map_height
                    case 8
                        xadd = -map_width
                        yadd = 0
                end select
                
                if particles(i).typ = 1 then rotozoomgl( ParticleTex, x+xadd*zoom, y+yadd*zoom, 0, zoom/1.6)
                if particles(i).typ = 2 then rotozoomgl( ParticleTex, x+xadd*zoom, y+yadd*zoom, 0, zoom/1.4)
                if particles(i).typ = 3 then rotozoomgl( ParticleTex, x+xadd*zoom, y+yadd*zoom, 0, zoom/3)
                
            next go_around_playfield
            
        end if
        
    next
    
end sub


sub Draw_Projectiles ( projectiles() as projectile_struct, plr() as entity_struct, byref loop_time as double)
    
    'this shit is totally retarded!
    'we now have almost 1800 lines of code in main?
    
    dim as integer xadd, yadd, xdistance, ydistance
    dim as matrix tmat
    
    dim as vec2f p1 = plr(1).object[0]->get_position
    dim as vec2f p2 = plr(2).object[0]->get_position
    
    XDistance = p1.X-p2.X
    YDistance = p1.Y-p2.Y
    
    if p1.X < map_width/2 and p2.X > map_width / 2 and abs(p1.X-p2.X) > abs(p1.X-(p2.X - map_width)) then XDistance = p1.X-(p2.X-map_width)
    if p1.X > map_width/2 and p2.X < map_width / 2 and abs(p1.X-p2.X) > abs((p1.X-map_width) - p2.X) then XDistance = (p1.X-map_width)-p2.X
    if p1.Y < map_height/2 and p2.Y > map_height / 2 and abs(p1.Y-p2.Y) > abs(p1.Y-(p2.Y - map_height)) then YDistance = p1.Y-(p2.Y-map_height)
    if p1.Y > map_height/2 and p2.Y < map_height / 2 and abs(p1.Y-p2.Y) > abs((p1.Y-map_height)- p2.Y) then YDistance = (p1.Y-map_height)-p2.Y
    
    Camera_CenterX = -(p1.X - ScreenXMid/zoom - (XDistance)/2)
    Camera_CenterY = -(p1.Y - ScreenYMid/zoom - (YDistance)/2)
    
    glEnable( GL_BLEND )
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
    
    
    for i as integer = 1 to max_num_of_projectiles
            if projectiles(i).active <> 0 then             
                glColor4f( 1,1,1,1 )
            elseif projectiles(i).explode<>0 then
                glColor4f( 1,1,0,.75 )
            end if
        
            tmat.LoadIdentity()
            tmat.Scale( zoom, zoom, 1f )
            tmat.Translate( Camera_CenterX, Camera_CenterY, 1f )
            if projectiles(i).active <> 0 then
                tmat.Translate( projectiles(i).body->p.x, projectiles(i).body->p.y, 0  )
            elseif projectiles(i).explode<>0 then
                tmat.Translate( projectiles(i).impact_position.x, projectiles(i).impact_position.y, 0  )
            end if
            '' Grab Our Particle X Position
            dim as single x = tmat.Position.X ' particles(i).x
            '' Grab Our Particle Y Position
            dim as single y = tmat.Position.y ' particles(i).y
            
            if projectiles(i).active <> 0 then
                rotozoomgl_imm( projectiles(i).sprite, x, y, projectiles(i).body->a * inv_pi_180, zoom, projectiles(i).Graphics_TileSize, projectiles(i).Graphics_TileSize, 2)
            elseif projectiles(i).explode<>0 then
                rotozoomgl( explode_tex, x, y, (projectiles(i).body->a+rnd*pi) * inv_pi_180, (projectiles(i).explosion/12) + zoom/100 )
            end if
            
            for go_around_playfield as integer = 1 to 8
                
                select case as const go_around_playfield
                    case 1
                        xadd = -map_width
                        yadd = -map_height
                    case 2
                        xadd = 0
                        yadd = -map_height
                    case 3
                        xadd = map_width
                        yadd = -map_height
                    case 4
                        xadd = map_width
                        yadd = 0
                    case 5
                        xadd = map_width
                        yadd = map_height
                    case 6
                        xadd = 0
                        yadd = map_height
                    case 7
                        xadd = -map_width
                        yadd = map_height
                    case 8
                        xadd = -map_width
                        yadd = 0
                end select
                
                if projectiles(i).active <> 0 then
                    rotozoomgl_imm( projectiles(i).sprite, x+xadd*zoom, y+yadd*zoom, projectiles(i).body->a * inv_pi_180, zoom, projectiles(i).Graphics_TileSize, projectiles(i).Graphics_TileSize, 2)
                elseif projectiles(i).explode<>0 then
                    rotozoomgl( explode_tex, x+xadd+zoom, y+yadd+zoom, (projectiles(i).body->a+rnd*pi) * inv_pi_180, (projectiles(i).explosion/12) + zoom/100 )
                end if
                
            
            next go_around_playfield
            
        'end if
        
    next
    
end sub


sub Draw_Comets ( comets() as projectile_struct, plr() as entity_struct, byref loop_time as double)
    
    'this shit is totally retarded!
    'we now have almost 1800 lines of code in main?
    
    dim as integer xadd, yadd, xdistance, ydistance
    dim as matrix tmat
    
    dim as vec2f p1 = plr(1).object[0]->get_position
    dim as vec2f p2 = plr(2).object[0]->get_position
    
    XDistance = p1.X-p2.X
    YDistance = p1.Y-p2.Y
    
    if p1.X < map_width/2 and p2.X > map_width / 2 and abs(p1.X-p2.X) > abs(p1.X-(p2.X - map_width)) then XDistance = p1.X-(p2.X-map_width)
    if p1.X > map_width/2 and p2.X < map_width / 2 and abs(p1.X-p2.X) > abs((p1.X-map_width) - p2.X) then XDistance = (p1.X-map_width)-p2.X
    if p1.Y < map_height/2 and p2.Y > map_height / 2 and abs(p1.Y-p2.Y) > abs(p1.Y-(p2.Y - map_height)) then YDistance = p1.Y-(p2.Y-map_height)
    if p1.Y > map_height/2 and p2.Y < map_height / 2 and abs(p1.Y-p2.Y) > abs((p1.Y-map_height)- p2.Y) then YDistance = (p1.Y-map_height)-p2.Y
    
    Camera_CenterX = -(p1.X - ScreenXMid/zoom - (XDistance)/2)
    Camera_CenterY = -(p1.Y - ScreenYMid/zoom - (YDistance)/2)
    
    glEnable( GL_BLEND )
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
    
    
    for i as integer = 1 to max_num_of_comets
            if comets(i).active <> 0 then             
                glColor4f( 1,1,1,1 )
            elseif comets(i).explode<>0 then
                glColor4f( 1,1,0,.75 )
            end if
        
            tmat.LoadIdentity()
            tmat.Scale( zoom, zoom, 1f )
            tmat.Translate( Camera_CenterX, Camera_CenterY, 1f )
            if comets(i).active <> 0 then
                tmat.Translate( comets(i).body->p.x, comets(i).body->p.y, 0  )
            elseif comets(i).explode<>0 then
                tmat.Translate( comets(i).impact_position.x, comets(i).impact_position.y, 0  )
            end if
            '' Grab Our Particle X Position
            dim as single x = tmat.Position.X ' particles(i).x
            '' Grab Our Particle Y Position
            dim as single y = tmat.Position.y ' particles(i).y
            
            if comets(i).active <> 0 then
                rotozoomgl_imm( comets(i).sprite, x, y, comets(i).body->a * inv_pi_180, zoom, comets(i).Graphics_TileSize, comets(i).Graphics_TileSize, 2)
            elseif comets(i).explode<>0 then
                rotozoomgl( explode_tex, x, y, (comets(i).body->a+rnd*pi) * inv_pi_180, (comets(i).explosion/12) + zoom/100 )
            end if
            
            for go_around_playfield as integer = 1 to 8
                
                select case as const go_around_playfield
                    case 1
                        xadd = -map_width
                        yadd = -map_height
                    case 2
                        xadd = 0
                        yadd = -map_height
                    case 3
                        xadd = map_width
                        yadd = -map_height
                    case 4
                        xadd = map_width
                        yadd = 0
                    case 5
                        xadd = map_width
                        yadd = map_height
                    case 6
                        xadd = 0
                        yadd = map_height
                    case 7
                        xadd = -map_width
                        yadd = map_height
                    case 8
                        xadd = -map_width
                        yadd = 0
                end select
                
                if comets(i).active <> 0 then
                    rotozoomgl_imm( comets(i).sprite, x+xadd*zoom, y+yadd*zoom, comets(i).body->a * inv_pi_180, zoom, comets(i).Graphics_TileSize, comets(i).Graphics_TileSize, 2)
                elseif comets(i).explode<>0 then
                    rotozoomgl( explode_tex, x+xadd+zoom, y+yadd+zoom, (comets(i).body->a+rnd*pi) * inv_pi_180, (comets(i).explosion/12) + zoom/100 )
                end if
                
            
            next go_around_playfield
            
        'end if
        
    next
    
end sub
