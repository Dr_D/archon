#include once "math/vecf.bi"

constructor vec2f ( byval x as single, byval y as single )
    
	this.x = x
	this.y = y
    
end constructor


constructor vec2f ( byref v2d as vec2f )
    
	this.x = v2d.x
	this.y = v2d.y
    
end constructor


constructor vec2f ( byref v2d as cpVect )
    
	this.x = v2d.x
	this.y = v2d.y
    
end constructor


constructor vec2f ( )
    
	this.x = 0.0
	this.y = 0.0
    
end constructor


function vec2f.dot ( byref v as vec2f ) as single
    
    return  this.x * v.x + this.y * v.y
    
end function


function vec2f.magnitude( ) as single
    
    dim Mag as single = any
    mag = sqr( this.x ^2 + this.y ^2 )
    if mag = 0 then mag = 1
    return mag
    
end function


sub vec2f.normalize()
    
	this = this / this.magnitude()
    
end sub


function vec2f.cross( byref v as vec2f ) as vec2f
    
    dim as vec2f crvec, rvec
    crvec = v - this
    dim as single vLen = crvec.magnitude
    rvec.x = ( crvec.y)  / vLen
    rvec.y = (-crvec.x)  / vLen
    return rvec
    
end function


function vec2f.cross_analog( byref v as vec2f ) as single
    return this.x*v.y-this.y*v.x
end function


function vec2f.distance( byref v as vec2f ) as single
    
	return sqr((v.x - this.x)^2 + (v.y - this.y)^2)
    
end function


function vec2f.AngleBetween( byref v as vec2f ) as single
    
	return acos( this.dot(v) / (this.magnitude * v.magnitude) )
    
end function


operator vec2f.cast() as cpVect
    
    return *cast (  cpVect ptr, @this )
    
end operator


operator vec2f.cast() as cpVect ptr
    
    return cast (  cpVect ptr, @this )
    
end operator


operator vec2f.cast() as string
    
	return "x: " & this.x & ", y: " & this.y
    
end operator


operator vec2f.let( byref v as cpVect )
    this.x = v.x
    this.y = v.y
end operator 


operator + ( byref lhs as vec2f, byref rhs as single ) as vec2f
    
    return type<vec2f>( lhs.x + rhs, lhs.y + rhs )
    
end operator


operator + ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f
    
	return type<vec2f>( lhs.x + rhs.x, lhs.y + rhs.y )
    
end operator

operator - ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f
    
	return type<vec2f>( lhs.x - rhs.x, lhs.y - rhs.y )
    
end operator

operator * ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f
    
	return type<vec2f>( lhs.x * rhs.x, lhs.y * rhs.y )
    
end operator


operator * ( byref lhs as vec2f, byref rhs as single ) as vec2f
    
	return type<vec2f>( lhs.x * rhs, lhs.y * rhs )
    
end operator 


operator * ( byref lhs as vec2f, byref rhs as double ) as vec2f
    
	return type<vec2f>( lhs.x * rhs, lhs.y * rhs )
    
end operator


operator / ( byref lhs as vec2f, byref rhs as integer ) as vec2f
    
	return type<vec2f>( lhs.x / rhs, lhs.y / rhs )
    
end operator


operator / ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f
    
	return type<vec2f>( lhs.x / rhs.x, lhs.y / rhs.y )
    
end operator


operator / ( byref lhs as vec2f, byref rhs as single ) as vec2f
    
    return type<vec2f>(lhs.x / rhs, lhs.y / rhs)
    
end operator

operator / ( byref lhs as vec2f, byref rhs as double ) as vec2f
    
    return type<vec2f>(lhs.x / rhs, lhs.y / rhs)
    
end operator
