#include once "math/vecf.bi"


type matrix
	
    public:
    
    declare constructor( byref c as single = 0)
    declare constructor( byref r as vec3f, byref u as vec3f, byref f as vec3f, byref p as vec3f )
    declare constructor( byref x as matrix )
    declare sub LoadIdentity()
    declare sub LookAt( byref v1 as vec3f, byref v2 as vec3f, byref vup as vec3f )
    declare function Inverse() as matrix
    declare function PlanarProjection( byref lightpos as vec4f, byref plane as vec4f ) as matrix
    declare sub AxisAngle( byref v as vec3f, byref angle as single )
    declare sub Translate( byref v as vec2f )
    declare sub Translate( byref v as vec3f )
    declare sub Translate( byref x as single, byref y as single, byref z as single )
    declare sub Translate( byref x as integer, byref y as integer, byref z as integer )
    declare sub Rotate( byref anglex as single, byref angley as single, byref anglez as single )
    declare sub Rotate( byref anglex as integer, byref angley as integer, byref anglez as integer )
    declare sub Scale( byref scalar as single )
    declare sub Scale( byref scalarx as single, byref scalary as single, byref scalarz as single )
    declare sub Gram_Schmidt( byref d as vec3f )
    
    declare property right( byref v as vec3f )
    declare property Up( byref v as vec3f )
    declare property Forward( byref v as vec3f )
    declare property Position( byref v as vec3f )
    declare property right() as vec3f
    declare property Up() as vec3f
    declare property Forward() as vec3f
    declare property Position() as vec3f
    declare property GetArrayData() as single ptr
    
    declare operator *= ( byref mat as matrix )
    declare operator cast() as string
    declare operator cast() as single ptr
    
    private:
    m(15) as single = any
    
end type

declare operator * ( byref lhs as matrix, byref rhs as matrix ) as matrix
declare operator * ( byref lhs as vec2f, byref rhs as matrix ) as vec2f
declare operator * ( byref lhs as vec3f, byref rhs as matrix ) as vec3f
declare operator * ( byref lhs as vec4f, byref rhs as matrix ) as vec4f