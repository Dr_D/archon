#include once "model.bi"

sub unload_model( byref Model as Model_struct ptr )
    
    deallocate( model->vertices )
    model->vertices = 0
    deallocate( model->triangles )
    model->triangles = 0
    deallocate( model )
    model = 0
    
end sub

sub load_model( byval Filename as string, byref Model as Model_struct ptr )
    dim as string   FileRec
    dim as uinteger FileID = freefile
    dim as uinteger vertex_counter
    dim as uinteger triangle_counter
    dim as uinteger uv_counter
    'dim as uinteger Red,Green,Blue,tcol

    '    if Model then
    '        print #debug, "Model already allocated!!!"
    '        exit sub
    '    end if
    '
    Model = callocate( sizeof( model_struct ) )

    open Filename for binary as #FileID
    do while not eof(FileID)
        line input #FileID, FileRec

        'Allocate our vertices
        if instr( FileRec, "Vertices:" ) then
            Model->max_vertices   = val(mid(FileRec, instr( FileRec, "Vertices:" ) + 10 ))
            'Model->vertices       = new vec2f[ Model->max_vertices ]
            Model->vertices       = callocate( Model->max_vertices * sizeof(vec2f) )
        end if

        'Allocate our triangles
        if instr( FileRec, "Faces:" ) then
            Model->max_triangles = val(mid(FileRec, instr( FileRec, "Faces:" ) + 7 ))
            'Model->triangles     = new triangle_struct[ Model->max_triangles ]
            Model->triangles     = callocate( Model->max_triangles* sizeof(triangle_struct) )
        end if


        'just a hackish way to stre colors for the triangles as a part of the surface name
        'it's like 000_000_000_name
        'where the first 3 0's depict the red intensity
        'where the next 3 depict the green...
        if mid( FileRec, 1, 6 ) = "usemtl" then
            'Red   = val(mid$( FileRec, 8 ) )
            'Green = val(mid$( FileRec, 12 ) )
            'Blue  = val(mid$( FileRec, 16 ) )
            'tcol = val(mid$( FileRec, 8 ) )
        end if


        select case mid( FileRec, 1, 2 )

            case "v "
                'process a vertex here
                'vertices are loaded straight into an array by themselves.
                dim as integer space_counter = 0
                for i as integer = 1 to len( FileRec )
                    if mid(FileRec, i, 1) = " " then
                        space_counter+=1
                        select case as const space_counter
                            case 1
                                'first space  = X
                                Model->vertices[vertex_counter].x = val(mid( FileRec, i+1 ) )
                            case 2
                                'second space = Y
                                Model->vertices[vertex_counter].y = val(mid( FileRec, i+1 ) )
                            case 3
                                'third space  = Z
                                'Model->vertices[vertex_counter].z = val(mid$( FileRec, i+1 ) )
                        end select
                    end if
                next
                'increment the vertex array counter
                vertex_counter+=1


            case "vt"
                uv_counter+=1
                if uv_counter>3 then uv_counter = 1
                'process uv's here
                dim as integer space_counter = 0
                for i as integer = 1 to len( FileRec )
                    if mid(FileRec, i, 1) = " " then
                        space_counter+=1
                        select case as const space_counter
                            case 1
                                'first space  = X
                                Model->triangles[triangle_counter].uv(4-uv_counter).x = val(mid( FileRec, i+1 ) )
                            case 2
                                'second space = Y
                                Model->triangles[triangle_counter].uv(4-uv_counter).y = val(mid( FileRec, i+1 ) )
                        end select
                    end if
                next
                'increment the vertex array counter



            case "f "
                'process a triangle/face here.
                'we have to subtract one because we're using pointers
                'and a pointer array is 0 relative.


                'Model->triangles[triangle_counter].col = tcol
                'Model->triangles[triangle_counter].r = Red
                'Model->triangles[triangle_counter].g = Green
                'Model->triangles[triangle_counter].b = Blue
                dim as integer space_counter = 0
                for i as integer = 1 to len( FileRec )
                    if mid(FileRec, i, 1) = " " then
                        space_counter+=1
                        Model->triangles[triangle_counter].point_id(space_counter) = val(mid( FileRec, i+1 ) )-1
                    end if
                next

                'increment the triangle index counter
                triangle_counter+=1

        end select
    loop

    close #FileID

    'calculate the normal for each triangle in the model
    'dim as vector3d tnorm(1 to 3)
    'for i as integer = 0 to Model->max_triangles-1
    '    tnorm(1) = Model->vertices[Model->triangles[i].point_ID(1)]
    '    tnorm(2) = Model->vertices[Model->triangles[i].point_ID(2)]
    '    tnorm(3) = Model->vertices[Model->triangles[i].point_ID(3)]
    '    Model->triangles[i].normal = poly_normal( @tnorm(1) )
    '    Model->triangles[i].normal.normalize
    'next

end sub