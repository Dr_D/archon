'#define FBMLD_NO_MULTITHREADING
'#include once "fbmld.bi"
#include once "GL/gl.bi"
#include once "math/mathf.bi"
#include once "math/vecf.bi"
#include once "math/matrix4x4f.bi"
#include once "chipmunk/chipmunk.bi"
#include once "model.bi"

enum object_type
    SHIP = 1
    TERRAIN
    PROJECTILE
    COMET
end enum

enum joint_type
    PIN = 1
    SLIDE
    PIVOT
    GROOVE
end enum


type joint_struct
    Joint       as cpJoint ptr
    pivot       as vec2f
    anchor_a    as vec2f
    anchor_b    as vec2f
    groove_a    as vec2f
    groove_b    as vec2f
    min_dist    as single
    max_dist    as single
    min_angle   as single
    max_angle   as single
end type


type object_struct
    'the main transformation matrix for this body
    tmatrix         as matrix
    'the ptr to the actual physics body for this object
    Body		    as cpBody ptr
    'the ptr to the chipmunk internal shape for this body(geometry)
    shape           as cpShape ptr ptr
    'the ptr to the raw geometry data
    model           as model_struct ptr
    'mass of the object
    mass            as single
    'tech id, used for our game id for this object
    tech_id         as integer
    'eforce, external force to apply to this body
    eForce		    as vec2f
    'epoint, point at the body at which to apply eforce
    ePoint          as vec2f
    'etorque, torque to apply to this body
    etorque         as single
    'start_position, the position the body is created at
    start_position  as vec2f
    'start_angle, the angle of rotation when the body is created
    start_angle     as single
    'returns from terrain callback if this body is resting on the ground
    on_ground       as integer
    
    is_world        as integer   
    
    collided        as integer
    
    taken_damage    as integer
    
    'this damage_id will hold the id of the projectile that caused the damage
    'we'll use it to know which projectile to destroy, and also animate the explosion
    damage_id       as integer
    hit_pos         as vec2f
    
    sprite              as gluint
    max_velocity        as single
    acceleration        as single
    accel_delta         as single
    WeaponPower         as single
    WeaponFireRatio     as single
    Second_WeaponPower  as single
    Hull                as integer
    Energy              as integer
    EnergyRecharge      as single
    Graphics_File       as string
    Model_File       	as string
    Graphics_TileSize   as integer
    StartWeaponX        as single
    StartWeaponY        as single
    StartThrustX        as single
    StartThrustY        as single
    ObjName         	as string
    turning_speed   	as single
    turn_delta          as single
    turn_damp           as single
    LightRadius			as single
    
    'sets the angle of rotation for this body in radians
    declare sub set_angle( byref a as single )
    'set the world position for this body in vec2f format
    declare sub set_position( byref v as vec2f )
    'set the world position for this body in x,y format
    declare sub set_position( byref x as single, byref y as single )

    'returns the angle from origin in radians
    declare function get_angle() as single
    
    'returns the actual rotated vector
    declare function get_rotvec() as vec2f
    
    'returns the world position of this body
    declare function get_position() as vec2f
    
    'returns the local position of this body
    declare function blp() as vec2f

end type


type projectile_struct
    
    graphics_tilesize   as integer
    sprite              as gluint
    start_position      as vec2f
    impact_position     as vec2f
    impact_force        as single
    position            as vec2f
    velocity            as vec2f
    Body		        as cpBody ptr
    shape               as cpShape ptr ptr
    model               as model_struct ptr
    mass                as single
    tech_id             as integer
    active              as integer
    explode             as integer
    explosion           as single
    
end type

type entity_struct
    space           as cpSpace ptr
    object          as object_struct ptr ptr
    max_objects     as integer
    joint           as joint_struct ptr ptr
    max_joints      as integer
    pptr            as projectile_struct ptr
end type


declare function ship_ship_collision_callback cdecl( byval a as cpShape ptr, byval b as cpShape ptr, byval contacts as cpContact ptr, byval numContacts as integer, byval normal_coef as cpFloat, byval dat as any ptr) as integer
declare function ship_terrain_collision_callback cdecl( byval a as cpShape ptr, byval b as cpShape ptr, byval contacts as cpContact ptr, byval numContacts as integer, byval normal_coef as cpFloat, byval dat as any ptr) as integer
declare function ship_projectile_collision_callback cdecl( byval a as cpShape ptr, byval b as cpShape ptr, byval contacts as cpContact ptr, byval numContacts as integer, byval normal_coef as cpFloat, byval dat as any ptr) as integer
