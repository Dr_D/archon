#Include once "gfx.bi"

sub psetgl2( byref x as single, byref y as single, byval c as uinteger )
    glMatrixMode( GL_MODELVIEW )
    glPushMatrix()
    glColor4F g2_color(c)
    'glDisable( GL_TEXTURE_2D )
    
    glBegin( GL_POINTS )
    glVertex2i( x, y )
    glEnd()
    glPopMatrix()
    'glEnable( GL_TEXTURE_2D )
end Sub

sub rotozoomgl( byref sprite as gluint, byref x as single, byref y as single, byref angle as integer, byref zoom as single )
    glMatrixMode( GL_MODELVIEW )
    glPushMatrix()
 
    glTranslateF( x, y, 0 )
    glRotateF( angle, 0, 0, 1 )
    glScaleF( zoom, zoom, 1 )
 
    glCallList( sprite )

    glPopMatrix()
    
end Sub
sub rotozoomgl_imm( byref sprite as gluint, byref x as single, byref y as single, byref angle as single, byref zoom as single, byref swidth as integer, byref sheight as integer, byref lightradius as Single)
    'this needs some serious clean-up. :\
    dim as vec2f light_pos = vec2f(x+lightradius, y+lightradius)
    dim as vec2f vpos
    dim as vec2f spos = vec2f(x-lightradius, y-lightradius)

    dim as single dist

    dim as matrix tnmat
    tnmat.LoadIdentity()
    tnmat.Rotate( 0f, 0f, Angle )

    static As vec2f verts(1 to 5)
    Static As vec2f tverts(1 to 5)
    Static As vec2f tcoord(1 to 5)
    Static As col_struct cols(1 to 5)
    verts(1) = vec2f(-swidth/2, sheight/2)
    verts(2) = vec2f(-swidth/2, -sheight/2)
    verts(3) = vec2f(0, 0)
    verts(4) = vec2f(swidth/2, -sheight/2)
    verts(5) = vec2f(swidth/2, sheight/2)
    tcoord(1) = vec2f(0,1)
    tcoord(2) = vec2f(0,0)
    tcoord(3) = vec2f(.5,.5)
    tcoord(4) = vec2f(1,0)
    tcoord(5) = vec2f(1,1)

    dim as triangle_struct tmodl(1 to 4)
    tmodl(1).point_id(1) = 1
    tmodl(1).point_id(2) = 2
    tmodl(1).point_id(3) = 3

    tmodl(2).point_id(1) = 2
    tmodl(2).point_id(2) = 4
    tmodl(2).point_id(3) = 3

    tmodl(3).point_id(1) = 4
    tmodl(3).point_id(2) = 5
    tmodl(3).point_id(3) = 3

    tmodl(4).point_id(1) = 5
    tmodl(4).point_id(2) = 1
    tmodl(4).point_id(3) = 3


    glMatrixMode( GL_MODELVIEW )
    glPushMatrix()
    

    glTranslateF( x, y, 0 )
    glRotateF( angle, 0,0, 1 )
    glScaleF( zoom, zoom, 1 )

    glBindTexture( GL_TEXTURE_2D, sprite )

    dim as vec2f tvec, dvec
    dim as single mag

    for i as integer = 1 to ubound(verts)
        tverts(i) = spos+verts(i)*tnmat.Inverse.Right
    next

    for i as integer = 1 to ubound(verts)
        dim as integer i2 = (i mod ubound(tverts) ) + 1
        dim as single tdist = tverts(i).Distance(light_pos)
        if tdist = 1 then tdist = 0
        dist = 1-((tdist^4)/25000000)
        cols(i) = type( dist, dist, dist, 1.0 )' yeah, crazy looking, but oh well. :p
    next


    'first triangle
    dim as integer i2
    for i as integer = 1 to ubound(tmodl)
        glBegin( GL_TRIANGLES )
        for p as integer = 1 to 3
            i2 = tmodl(i).point_id(p)
            glColor4fv( @cols(i2).r )
            glTexCoord2fv( @tcoord(i2).x )
            glVertex2fv( @verts(i2).x )
        next
        glEnd()
    next

    glColor3f(1,1,1)
    glPopMatrix()
end sub
