#Include once "physics.bi"


function Ship_Ship_Collision_Callback cdecl( byval a as cpShape ptr, byval b as cpShape ptr, byval contacts as cpContact ptr, byval numContacts as integer, byval normal_coef as cpFloat, byval dat as any ptr ) as integer

    dim as object_struct ptr objecta
    dim as object_struct ptr objectb
    objecta = cast( object_struct ptr, a->dat )
    objectb = cast( object_struct ptr, b->dat )
    a->e = 0.001
    b->e = 0.001
    a->u = 10.001
    b->u = 10.001

    objecta->collided = 1
    objectb->collided = 1
    
    for i as integer = 0 to numContacts-1
        'objecta->hit_pos = contacts[i].p
        'print #debug, numContacts
    next

    return 1
end function


function Ship_Terrain_Collision_Callback cdecl( byval a as cpShape ptr, byval b as cpShape ptr, byval contacts as cpContact ptr, byval numContacts as integer, byval normal_coef as cpFloat, byval dat as any ptr ) as integer

    dim as object_struct ptr objecta
    objecta = cast( object_struct ptr, a->dat )
    b->e = 0.995
    b->u = 0.15
    'b->surface_v = cpv(10000.0,0) 'hehehe conveyor belts? :p

    for i as integer = 0 to numContacts-1
        'objecta->hit_pos = contacts[i].p
        objecta->on_ground = 1
        'print #debug, numContacts
    next

    return 1
end function



function Ship_Projectile_Collision_Callback cdecl( byval a as cpShape ptr, byval b as cpShape ptr, byval contacts as cpContact ptr, byval numContacts as integer, byval normal_coef as cpFloat, byval dat as any ptr ) as integer

    dim as object_struct ptr objecta
    dim as projectile_struct ptr projecta
    objecta = cast( object_struct ptr, a->dat )
    projecta = cast( projectile_struct ptr, b->dat )
    
    b->e = 0.995
    b->u = 0.15

    for i as integer = 0 to numContacts-1
        'if projecta->active <> 0 then
            'objecta->on_ground = 1
            objecta->taken_damage = 1
            objecta->damage_id = projecta->tech_id
            objecta->hit_pos = contacts[i].p
            projecta->impact_position = contacts[i].p
            'projecta->damage_id = objecta->tech_id
            
            'projecta->body->p = projecta->start_position 
            'print #debug, numContacts
        'end if
        'return 1
    next
    
    'if projecta->active <> 0 then
        'return 1
    'else 
        return 0
    'end if
end function



sub object_struct.set_position( byref v as vec2f )
    this.Body->p.x = v.x
    this.Body->p.y = v.y
end sub


sub object_struct.set_position( byref x as single, byref y as single )
    this.Body->p.x = x
    this.Body->p.y = y
end sub


sub object_struct.set_angle( byref a as single )
    cpBodySetAngle( this.Body, a )
end sub


function Object_Struct.get_position() as vec2f
    return vec2f(this.Body->p)
end function


function Object_Struct.get_rotvec() as vec2f
    return vec2f(this.Body->rot)
end function

function object_struct.get_angle() as single
    return this.Body->a
end function


function Object_Struct.blp() as vec2f
    return cpBodyWorld2Local( this.Body, this.Body->p )
end function