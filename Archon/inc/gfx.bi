'#define FBMLD_NO_MULTITHREADING
'#include once "fbmld.bi"
#include once "GL/gl.bi"
#include once "GL/glu.bi"
#include once "GL/glext.bi"
#include once "math/mathf.bi"
#include once "math/vecf.bi"
#include once "math/matrix4x4f.bi"
#include once "model.bi"

#define g2_color(c) ((c and &HFF0000) shr 16) / 255, ((c and &HFF00) shr 8) / 255, (c and &HFF) / 255, ((c and &HFF000000) shr 24) / 255

enum
TEX_NEAREST = 1
TEX_MIPMAP
end enum

type col_struct
    as single r,g,b,a
end type

type sprite_struct
    sprite      as gluint
    position    as vec2f
    mcolor      As UInteger
    typ         As integer   
end type

type particle_struct
    as integer active '' Active (Yes/No)
    as double  life   '' Particle Life  
    as double  fade   '' Fade Speed     
 
    as single  r      '' Red Value      
    as single  g      '' Green Value    
    as single  b      '' Blue Value     
 
    as single  x      '' X Position     
    as single  y      '' Y Position     
    as single  z      '' Z Position     
 
    as single  xi     '' X Direction    
    as single  yi     '' Y Direction    
    as single  zi     '' Z Direction    
 
    as single  xg     '' X Gravity      
    as single  yg     '' Y Gravity      
    as single  zg     '' Z Gravity  
    as integer typ
end type

'' Rainbow of colors
dim shared as GLfloat colors_table (0 to 11, 0 to 2) = { _
{ 1.0f,  0.5f,  0.5f}, _
{ 1.0f,  0.75f, 0.5f}, _
{ 1.0f,  1.0f,  0.5f}, _
{ 0.75f, 1.0f,  0.5f}, _
{ 0.5f,  1.0f,  0.5f}, _
{ 0.5f,  1.0f,  0.75f}, _
{ 0.5f,  1.0f,  1.0f}, _
{ 0.5f,  0.75f, 1.0f}, _
{ 0.5f,  0.5f,  1.0f}, _
{ 0.75f, 0.5f,  1.0f}, _
{ 1.0f,  0.5f,  1.0f}, _
{ 1.0f,  0.5f,  0.75f} }

declare sub psetgl2( byref x as single, byref y as single, byval c as uinteger )
declare sub rotozoomgl_imm( byref sprite as gluint, byref x as single, byref y as single, byref angle as single, byref zoom as single, byref swidth as integer, byref sheight as integer,  byref lightradius as single)
declare sub rotozoomgl( byref sprite as gluint, byref x as single, byref y as single, byref angle as integer, byref zoom as single )