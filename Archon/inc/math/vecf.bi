type matrix_ as matrix
#include once "chipmunk/chipmunk.bi"

type vec2f
    
	declare constructor ( byval x as single, byval y as single )
	declare constructor ( byref v2d as vec2f )
	declare constructor ( byref v2d as cpVect )
	declare constructor ( )
    
    x as single
    y as single
    
    declare operator cast() as cpVect
    declare operator cast() as cpVect ptr
    
    declare operator cast() as string
    
    declare operator let( byref v as cpVect )
    
    declare function dot ( byref v as vec2f ) as single
    declare function magnitude() as single
    declare sub normalize()
    declare function cross( byref v as vec2f ) as vec2f
    declare function cross_analog( byref v as vec2f ) as single
    declare function distance( byref v as vec2f ) as single
    declare function AngleBetween( byref v as vec2f ) as single
    
end type


type vec3f
    
	declare constructor ( byval x as single, byval y as single, byval z as single )
	declare constructor ( byref v3d as vec3f )
	declare constructor ( )
    
    x as single
    y as single
    z as single
    
    declare operator Cast() as string
    declare operator Cast() as vec2f
    declare operator *= ( byref rhs as matrix_ )
    declare operator *= ( byref s as single )
    
    declare operator +=( byref rhs as vec3f )
    declare function dot ( byref v as vec3f ) as single
    declare function magnitude() as single
    declare sub normalize()
    declare function cross( byref v as vec3f ) as vec3f
    declare function distance( byref v as vec3f ) as single
    declare function AngleBetween( byref v as vec3f ) as single
    
end type


type vec4f
    
	declare constructor ( byval x as single, byval y as single, byval z as single, byval w as double )
	declare constructor ( byref v4d as vec4f )
	declare constructor ( )
    
    x as single
    y as single
    z as single
    w as single
    
    declare operator Cast() as string
    declare operator Cast() as vec3f
    declare operator *= ( byref rhs as matrix_ )
    
    declare function dot ( byref v as vec4f ) as single
    declare function magnitude() as single
    declare sub normalize()
    declare function cross( byref v as vec4f ) as vec4f
    declare function distance ( byref v as vec4f ) as single
    
end type


declare operator + ( byref lhs as vec2f, byref rhs as single ) as vec2f
declare operator + ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f



declare operator + ( byref lhs as vec3f, byref rhs as single ) as vec3f
declare operator + ( byref lhs as vec3f, byref rhs as vec3f ) as vec3f

declare operator + ( byref lhs as vec3f, byref rhs as double ) as vec4f
declare operator + ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f
declare operator + ( byref lhs as vec4f, byref rhs as single ) as vec4f



declare operator - ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f

declare operator - ( byref lhs as vec3f ) as vec3f
declare operator - ( byref lhs as vec3f, byref rhs as vec3f ) as vec3f
declare operator - ( byref lhs as vec3f, byref rhs as vec4f ) as vec3f
declare operator - ( byref lhs as vec3f, byref rhs as single ) as vec3f

declare operator - ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f



declare operator * ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f
declare operator * ( byref lhs as vec2f, byref rhs as single ) as vec2f
declare operator * ( byref lhs as vec2f, byref rhs as double ) as vec2f

declare operator * ( byref lhs as vec3f, byref rhs as vec3f ) as vec3f
declare operator * ( byref lhs as vec3f, byref rhs as vec4f ) as vec3f
declare operator * ( byref lhs as vec3f, byref rhs as double ) as vec3f
declare operator * ( byref lhs as vec3f, byref rhs as single ) as vec3f

declare operator * ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f
declare operator * ( byref lhs as vec4f, byref rhs as single ) as vec4f



declare operator / ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f
declare operator / ( byref lhs as vec2f, byref rhs as integer  ) as vec2f
declare operator / ( byref lhs as vec2f, byref rhs as single   ) as vec2f
declare operator / ( byref lhs as vec2f, byref rhs as double   ) as vec2f


declare operator / ( byref lhs as vec3f, byref rhs as vec3f ) as vec3f
declare operator / ( byref lhs as vec3f, byref rhs as single ) as vec3f
declare operator / ( byref lhs as vec3f, byref rhs as double ) as vec3f


declare operator / ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f
declare operator / ( byref lhs as vec4f, byref rhs as single ) as vec4f