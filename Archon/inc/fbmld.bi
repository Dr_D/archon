''
'' fbmld (FB Memory Leak Detector) version 0.6
'' Copyright (C) 2006 Daniel R. Verkamp
'' Tree storage implemented by yetifoot
''
'' This software is provided 'as-is', without any express or implied warranty.
'' In no event will the authors be held liable for any damages arising from
'' the use of this software.
''
'' Permission is granted to anyone to use this software for any purpose,
'' including commercial applications, and to alter it and redistribute it
'' freely, subject to the following restrictions:
''
'' 1. The origin of this software must not be misrepresented; you must not claim
'' that you wrote the original software. If you use this software in a product,
'' an acknowledgment in the product documentation would be appreciated but is
'' not required.
''
'' 2. Altered source versions must be plainly marked as such, and must not be
'' misrepresented as being the original software.
''
'' 3. This notice may not be removed or altered from any source
'' distribution.
''

#ifndef __FBMLD__
#define __FBMLD__

#include "crt.bi"

#undef allocate
#undef callocate
#undef reallocate
#undef deallocate

#define allocate(bytes) fbmld_allocate((bytes), __FILE__, __LINE__)
#define callocate(bytes) fbmld_callocate((bytes), __FILE__, __LINE__)
#define reallocate(pt, bytes) fbmld_reallocate((pt), (bytes), __FILE__, __LINE__, #pt)
#define deallocate(pt) fbmld_deallocate((pt), __FILE__, __LINE__, #pt)

Type fbmld_t
        pt      As Any Ptr
        bytes   As Uinteger
        file    As String
        linenum As Integer
        Left    As fbmld_t Ptr
        Right   As fbmld_t Ptr
End Type

Common Shared fbmld_tree As fbmld_t Ptr
Common Shared fbmld_mutex As Any Ptr
Common Shared fbmld_instances As Integer

Private Sub fbmld_print(Byref s As String)
        fprintf(stderr, "(FBMLD) " & s & Chr(10))
End Sub
 
Private Sub fbmld_mutexlock( )
#ifndef FBMLD_NO_MULTITHREADING
        mutexlock(fbmld_mutex)
#endif
End Sub

Private Sub fbmld_mutexunlock( )
#ifndef FBMLD_NO_MULTITHREADING
        mutexunlock(fbmld_mutex)
#endif
End Sub

 
Private Function new_node _
        ( _
                Byval pt      As Any Ptr, _
                Byval bytes   As Uinteger, _
                Byref file    As String, _
                Byval linenum As Integer _
        ) As fbmld_t Ptr

        Dim As fbmld_t Ptr node = calloc(1, sizeof(fbmld_t))

        node->pt = pt
        node->bytes = bytes
        node->file = file
        node->linenum = linenum
        node->left = NULL
        node->right = NULL
       
        Function = node

End Function
 
Private Sub free_node _
        ( _
                Byval node As fbmld_t Ptr _
        )

        node->file = ""
        free( node )

End Sub

Private Function fbmld_search _
        ( _
                Byval root    As fbmld_t Ptr ptr, _
                Byval pt      As Any Ptr _
        ) As fbmld_t Ptr ptr

        Dim As fbmld_t Ptr ptr node = root
        Dim As Any Ptr a = pt, b = Any
       
        asm
                mov eax, dword Ptr [a]
                bswap eax
                mov dword Ptr [a], eax
        End asm
       
        While *node <> NULL
                b = (*node)->pt
                asm
                        mov eax, dword Ptr [b]
                        bswap eax
                        mov dword Ptr [b], eax
                End asm
                If a < b Then
                        node = @(*node)->left
                Elseif a > b Then
                        node = @(*node)->right
                Else
                        Exit While
                End If
        Wend
       
        Function = node

End Function

Private Sub fbmld_insert _
        ( _
                Byval root    As fbmld_t Ptr ptr, _
                Byval pt      As Any Ptr, _
                Byval bytes   As Uinteger, _
                Byref file    As String, _
                Byval linenum As Integer _
        )

        Dim As fbmld_t Ptr ptr node = fbmld_search(root, pt)

        If *node = NULL Then
                *node = new_node( pt, bytes, file, linenum )
        End If

End Sub

Private Sub fbmld_swap _
        ( _
                Byval node1 As fbmld_t Ptr ptr, _
                Byval node2 As fbmld_t Ptr ptr _
        )

        Swap (*node1)->pt,      (*node2)->pt
        Swap (*node1)->bytes,   (*node2)->bytes
        Swap (*node1)->file,    (*node2)->file
        Swap (*node1)->linenum, (*node2)->linenum

End Sub

Private Sub fbmld_delete _
        ( _
                Byval node As fbmld_t Ptr ptr _
        )

        Dim As fbmld_t Ptr old_node = *node
        Dim As fbmld_t Ptr ptr pred

        If (*node)->left = NULL Then
                *node = (*node)->right
                free_node( old_node )
        Elseif (*node)->right = NULL Then
                *node = (*node)->left
                free_node( old_node )
        Else
                pred = @(*node)->left
                While (*pred)->right <> NULL
                        pred = @(*pred)->right
                Wend
                fbmld_swap( node, pred )
                fbmld_delete( pred )
        End If

End Sub

Private Sub fbmld_init _
        ( _
        ) Constructor 101

        If fbmld_instances = 0 Then
#ifndef FBMLD_NO_MULTITHREADING
                fbmld_mutex = mutexcreate()
#endif
        End If
        fbmld_instances += 1
End Sub

Private Sub fbmld_tree_clean _
        ( _
                Byval node As fbmld_t Ptr ptr _
        )

        If *node <> NULL Then
                fbmld_tree_clean( @((*node)->left) )
                fbmld_tree_clean( @((*node)->right) )
                fbmld_print( "error: " & (*node)->bytes & " bytes allocated at " & (*node)->file & ":" & (*node)->linenum & " [&H" & Hex( (*node)->pt, 8 ) & "] not deallocated" )
                (*node)->file = ""
                free( (*node)->pt )
                free( *node )
                *node = NULL
        End If
End Sub

Private Sub fbmld_exit _
        ( _
        ) Destructor 101

        fbmld_instances -= 1
       
        If fbmld_instances = 0 Then
               
                If fbmld_tree <> NULL Then
                        fbmld_print("---- memory leaks ----")
                        fbmld_tree_clean(@fbmld_tree)
                Else
                        fbmld_print("all memory deallocated")
                End If
               
#ifndef FBMLD_NO_MULTITHREADING
                If fbmld_mutex <> 0 Then
                        mutexdestroy(fbmld_mutex)
                        fbmld_mutex = 0
                End If
#endif
       
        End If
End Sub

Private Function fbmld_allocate(Byval bytes As Uinteger, Byref file As String, Byval linenum As Integer) As Any Ptr
        Dim ret As Any Ptr = Any
       
        fbmld_mutexlock()
       
        If bytes = 0 Then
                fbmld_print("warning: allocate(0) called at " & file & ":" & linenum & "; returning NULL")
                ret = 0
        Else
                ret = malloc(bytes)
                fbmld_insert(@fbmld_tree, ret, bytes, file, linenum)
        End If
       
        fbmld_mutexunlock()
       
        Return ret
End Function

Private Function fbmld_callocate(Byval bytes As Uinteger, Byref file As String, Byval linenum As Integer) As Any Ptr
        Dim ret As Any Ptr = Any
       
        fbmld_mutexlock()
       
        If bytes = 0 Then
                fbmld_print("warning: callocate(0) called at " & file & ":" & linenum & "; returning NULL")
                ret = 0
        Else
                ret = calloc(1, bytes)
                fbmld_insert(@fbmld_tree, ret, bytes, file, linenum)
        End If
       
        fbmld_mutexunlock()
       
        Return ret
End Function

Private Function fbmld_reallocate(Byval pt As Any Ptr, Byval bytes As Uinteger, Byref file As String, Byval linenum As Integer, Byref varname As String) As Any Ptr
        Dim ret As Any Ptr = Any
        Dim node As fbmld_t Ptr ptr = Any
       
        fbmld_mutexlock()
       
        node = fbmld_search(@fbmld_tree, pt)
       
        If pt = NULL Then
                If bytes = 0 Then
                        fbmld_print("error: reallocate(" & varname & " [NULL] , 0) called at " & file & ":" & linenum)
                        ret = NULL
                Else
                        ret = malloc(bytes)
                        fbmld_insert(@fbmld_tree, ret, bytes, file, linenum)
                End If
        Elseif *node = NULL Then
                fbmld_print("error: invalid reallocate(" & varname & " [&H" & Hex(pt, 8) & "] ) at " & file & ":" & linenum)
                ret = NULL
        Elseif bytes = 0 Then
                fbmld_print("warning: reallocate(" & varname & " [&H" & Hex(pt, 8) & "] , 0) called at " & file & ":" & linenum & "; deallocating")
                free(pt)
                If *node <> NULL Then fbmld_delete(node)
                ret = NULL
        Else
                ret = realloc(pt, bytes)
               
                If ret = pt Then
                        (*node)->bytes = bytes
                        (*node)->file = file
                        (*node)->linenum = linenum
                Else
                        fbmld_delete(node)
                        fbmld_insert(@fbmld_tree, ret, bytes, file, linenum)
                End If
        End If
       
        fbmld_mutexunlock()
       
        Return ret
End Function

Private Sub fbmld_deallocate(Byval pt As Any Ptr, Byref file As String, Byval linenum As Integer, Byref varname As String)
        Dim node As fbmld_t Ptr ptr
       
        fbmld_mutexlock()
       
        If pt = NULL Then
                fbmld_print("warning: deallocate(" & varname & " [NULL] ) at " & file & ":" & linenum)
        Else
                node = fbmld_search(@fbmld_tree, pt)
               
                If *node = NULL Then
                        fbmld_print("error: invalid deallocate(" & varname & " [&H" & Hex(pt, 8) & "] ) at " & file & ":" & linenum)
                Else
                        fbmld_delete(node)
                        free(pt)
                End If
        End If
       
        fbmld_mutexunlock()
End Sub

#endif '' __FBMLD__
 