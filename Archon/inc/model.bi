'#define FBMLD_NO_MULTITHREADING
'#include once "fbmld.bi"
#include once "math/vecf.bi"

type triangle_struct
    point_ID(1 to 3) as integer
    uv(1 to 3)       as vec2f
end type

type model_struct
    max_vertices    as integer
    vertices        as vec2f ptr
    max_triangles   as integer
    triangles       as triangle_struct ptr
end type


declare sub load_model( byval Filename as string, byref Model as Model_struct ptr )
declare sub unload_model( byref Model as Model_struct ptr )