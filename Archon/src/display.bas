#include once "display.bi"

sub set_ortho( byval W as integer, byval H as integer )
	glmatrixmode( Gl_PROJECTION )
	glpushmatrix()
	glloadidentity()
	glmatrixmode( Gl_MODELVIEW )
	glpushmatrix()
	glloadidentity()
	glviewport( 0, 0, W, H )
	glortho( 0, W, 0, H, -1, 1 )
end sub


sub drop_ortho()
	glmatrixmode( Gl_PROJECTION )
	glpopmatrix()
	glmatrixmode( Gl_MODELVIEW )
	glpopmatrix()
end sub


sub make_font_dlist( byref DList as GlUINT,  byval Wtiles as single, byval Htiles as single, byval t_Scale as integer )
	DList = glgenlists(256)
	dim as integer i
	dim as single Tx, Ty, XStepp = 1/Wtiles, YStepp = 1/Htiles
	glmatrixmode( GL_MODELVIEW )
	glloadidentity()
	glpushmatrix()
    
	i=0
	for TY as integer = Htiles-1 to 0 step -1
		for TX as integer = 0 to Wtiles-1
			glnewlist DList+i, GL_COMPILE
            'glScaleF( 1, -1, 1 )
			glbegin GL_QUADS

			gltexcoord2d Tx*XStepp, (Ty+1)*YStepp
			glvertex2i 0, t_Scale

			gltexcoord2d Tx*XStepp, Ty*YStepp
			glvertex2i 0, 0

			gltexcoord2d (Tx+1)*XStepp, Ty*YStepp
			glvertex2i t_Scale, 0

			gltexcoord2d (Tx+1)*XStepp, (Ty+1)*YStepp
			glvertex2i t_Scale, t_Scale
                
			glend
            'glScaleF( 1, -1, 1 )
			gltranslatef t_Scale, 0, 0
			glendlist
			i+=1
		next
	next
	glpopmatrix()
end sub


sub glprint(byval Strng as string, byval Texture as Gluint, byval Text_List as GLUINT, byval X as single, byval Y as single, byval R as single, byval G as single, byval B as single )
	dim as integer i, Length = len(Strng)
	glenable( GL_TEXTURE_2D )
	glbindtexture( GL_TEXTURE_2D, Texture )
	glenable( GL_BLEND )
	glblendfunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
	'glpolygonmode( GL_FRONT, GL_FILL )
	glmatrixmode( GL_PROJECTION )
	glpushmatrix()
	glloadidentity()
	glmatrixmode( GL_MODELVIEW )
	glpushmatrix()
	glloadidentity()
	glortho( 0,Display.W, 0, Display.H, -1, 1 )

	gltranslatef( X, Y, 0 )
	glcolor3f( R,G,B )
	gllistbase( Text_List )
	glcalllists( Length, GL_UNSIGNED_BYTE, strptr(Strng) )

	glmatrixmode( GL_PROJECTION )
	glpopmatrix()
	glmatrixmode( GL_MODELVIEW )
	glpopmatrix()
	
end sub


function open_gl_window( byval W as integer, byval H as integer, byval BPP as integer, byval Num_buffers as integer, byval Num_Samples as integer, byval Fullscreen as integer ) as integer
	dim Flags as integer = FB.GFX_OPENGL or FB.GFX_HIGH_PRIORITY
	if FullScreen then
		Flags or = FB.GFX_FULLSCREEN
	end if
	screencontrol FB.SET_GL_COLOR_BITS, BPP
	screencontrol FB.SET_GL_DEPTH_BITS, 24
	screencontrol FB.SET_GL_STENCIL_BITS, 8
	if Num_Samples>0 then
		screencontrol FB.SET_GL_NUM_SAMPLES, Num_Samples
		Flags or = FB.GFX_MULTISAMPLE
	end if
	screenres W, H, BPP, Num_Buffers, Flags' or FB.GFX_STENCIL_BUFFER
    
    debug = freefile
    open cons for output as #debug
    
	glViewport( 0, 0, W, H )
	glMatrixmode( GL_PROJECTION )
	glloadidentity()
	Display.W = W
	Display.H = H
	Display.W2 = W\2
	Display.H2 = H\2
	Display.BPP = BPP
	Display.FOV = 60
	Display.Aspect = W/H
	Display.znear = 1
	Display.zfar = 5000
	gluPerspective( Display.FOV, Display.Aspect, Display.zNear, Display.zFar )

	glClearColor( 0.0, 0.0, 0.0, 1.0 )
	glClearDepth( 1.0 )
	glDisable( GL_DEPTH_TEST )
   glDisable( GL_LIGHTING )
	glDepthFunc( GL_LEQUAL )
	glEnable( GL_COLOR_MATERIAL )

	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST )
	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )
	glEnable( GL_CULL_FACE )
	glMatrixMode( GL_MODELVIEW )
	glShadeModel( GL_SMOOTH )
	gather_extensions()
		
	return 1
end function


sub gather_extensions()

	dim extensions as string
	screencontrol FB.GET_GL_EXTENSIONS, extensions

	dim as integer i, j

	do
		i+=1
		j=i
		dim as string tstring
		dim as string char
		do
			char = mid(extensions,j,1)
			tstring+=char
			j=j+1
			if char = " " then
				exit do
			end if
		loop while j<len(extensions)

		i = j-1
		print #50, tstring

	loop while i<len(extensions)


	if (instr(extensions, "GL_EXT_texture_filter_anisotropic") <> 0) then
		_anisotropic_ = 1
	end if

	if (instr(extensions, "GL_EXT_framebuffer_object") <> 0) then
		print #50, "GL_EXT_framebuffer_object is supported"
		_framebuffer_ = 1
		glGenFramebuffersEXT            = screenglproc("glGenFramebuffersEXT")
		glDeleteFramebuffersEXT         = screenglproc("glDeleteFramebuffersEXT")
		glBindFramebufferEXT            = screenglproc("glBindFramebufferEXT")
		glFramebufferTexture2DEXT       = screenglproc("glFramebufferTexture2DEXT")
		glFramebufferRenderbufferEXT    = screenglproc("glFramebufferRenderbufferEXT")
		glGenRenderbuffersEXT           = screenglproc("glGenRenderbuffersEXT")
		glBindRenderbufferEXT           = screenglproc("glBindRenderbufferEXT")
		glRenderbufferStorageEXT        = screenglproc("glRenderbufferStorageEXT")
	else
		print #50, "GL_EXT_framebuffer_object is NOT supported"
	end if


	if (instr(extensions, "GL_ARB_multitexture") <> 0) then
		print #50, "GL_ARB_multitexture is supported"
		_multitexture_ = 1
		glMultiTexCoord2fARB            = screenglproc("glMultiTexCoord2fARB")
		glMultiTexCoord2fvARB           = screenglproc("glMultiTexCoord2fvARB")
		glActiveTextureARB              = screenglproc("glActiveTextureARB")
		glClientActiveTextureARB        = screenglproc("glClientActiveTextureARB")
		glgetintegerv( GL_MAX_TEXTURE_UNITS_ARB, @maxTexelUnits )
		print #50, maxTexelUnits & " Texture units supported"
		if maxTexelUnits<3 then
			_multitexture_ = 0
			print #50, "Insufficient texture units for multitexture support"
		end if

	else
		print #50, "GL_ARB_multitexture is NOT supported"
	end if


	if (instr(extensions, "GL_ARB_shading_language_100") <> 0) then
		print #50, "GL_ARB_shading_language_100 is supported"
		if (instr(extensions, "GL_ARB_vertex_program") <> 0) then
			print #50, "GL_ARB_vertex_program is supported"
			if (instr(extensions, "GL_ARB_fragment_program") <> 0) then
				print #50, "GL_ARB_fragment_program is supported"
				_shader100_ = 1
				glCreateShaderObjectARB     = screenglproc("glCreateShaderObjectARB")
				glShaderSourceARB           = screenglproc("glShaderSourceARB")
				glGetShaderSourceARB        = screenglproc("glGetShaderSourceARB")
				glCompileShaderARB          = screenglproc("glCompileShaderARB")
				glDeleteObjectARB           = screenglproc("glDeleteObjectARB")
				glCreateProgramObjectARB    = screenglproc("glCreateProgramObjectARB")
				glAttachObjectARB           = screenglproc("glAttachObjectARB")
				glUseProgramObjectARB       = screenglproc("glUseProgramObjectARB")
				glLinkProgramARB            = screenglproc("glLinkProgramARB")
				glValidateProgramARB        = screenglproc("glValidateProgramARB")
				glGetInfoLogARB             = screenglproc("glGetInfoLogARB")
				glGetObjectParameterivARB   = screenglproc("glGetObjectParameterivARB")
				glGetUniformLocationARB     = screenglproc("glGetUniformLocationARB")
				glUniform1iARB              = screenglproc("glUniform1iARB")
				glUniform1fARB              = screenglproc("glUniform1fARB")
				glUniform2fvARB             = screenglproc("glUniform2fvARB")
				glUniform3fvARB             = screenglproc("glUniform3fvARB")
			else
				print #50, "GL_ARB_fragment_program is NOT supported"
			end if
		else
			print #50, "GL_ARB_vertex_program is NOT supported"
		end if
	else
		print #50, "GL_ARB_shading_language_100 is NOT supported"
	end if


end sub


function init_shader( File_Name as string, Shader_Type as integer )as GlHandleARB

	dim as integer i
	dim as integer Line_Cnt
	dim as string Shader_Text, tString
	dim as Gluint Shader_Compile_Success
	dim as GlHandleARB Shader
	dim as uinteger FileNum = freefile

	open File_Name for binary as #FileNum
	do while not eof(FileNum)
		line input #FileNum, tString
		Shader_Text += tString + chr( 13, 10 )
	loop
	close #FileNum

	dim as GLcharARB ptr table(0) => { strptr( Shader_Text ) }
	Shader = glCreateShaderObjectARB( Shader_Type )
	glShaderSourceARB( Shader, 1, @table(0), 0 )
	glCompileShaderARB( Shader )

	glGetObjectParameterivARB( Shader, GL_OBJECT_COMPILE_STATUS_ARB, @Shader_Compile_Success )
	if Shader_Compile_Success = 0 then
		dim as Gluint infologsize
		glGetObjectParameterivARB( Shader, GL_OBJECT_INFO_LOG_LENGTH_ARB, @infoLogSize)
		dim as GlByte infolog(InfoLogSize)
		glGetInfoLogARB( Shader, InfoLogSize, 0, @infoLog(0))
		tString=""
		for i = 0 to InfoLogSize-1
			tString+=chr(InfoLog(i))
		next
		print #50, "Shader Infolog error message:"
		print #50, tString
		return 0
	else
		return Shader
	end if

end function
