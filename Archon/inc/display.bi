'#define FBMLD_NO_MULTITHREADING
'#include once "fbmld.bi"
#include once "\GL\gl.bi"
#include once "\GL\glu.bi"
'#include once "\GL\glfw.bi"
#include once "\GL\glext.bi"
#include once "fbgfx.bi"

type Display_Struct
	W as integer
	H as integer
	w2 as integer
	h2 as integer
	BPP as integer
	MODE as uinteger
	GlVer as zstring ptr
	as single FOV, Aspect, zNear, zFar
end type

common shared Display as Display_Struct
common shared debug as integer 

'for framebuffers
common shared _framebuffer_                    as integer
common shared glGenFramebuffersEXT             as PFNglGenFramebuffersEXTPROC
common shared glDeleteFramebuffersEXT          as PFNglDeleteFramebuffersEXTPROC
common shared glBindFramebufferEXT             as PFNglBindFramebufferEXTPROC
common shared glFramebufferTexture2DEXT        as PFNglFramebufferTexture2DEXTPROC
common shared glFramebufferRenderbufferEXT     as PFNglFramebufferRenderbufferEXTPROC
common shared glGenRenderbuffersEXT            as PFNglGenRenderbuffersEXTPROC
common shared glBindRenderbufferEXT            as PFNglBindRenderbufferEXTPROC
common shared glRenderbufferStorageEXT         as PFNglRenderbufferStorageEXTPROC

'for multitexture
common shared _multitexture_                    as integer
common shared maxTexelUnits                     as Gluint
common shared glMultiTexCoord2fARB              as PFNglMultiTexCoord2fARBPROC
common shared glMultiTexCoord2fvARB             as PFNglMultiTexCoord2fvARBPROC
common shared glActiveTextureARB                as PFNGlActiveTextureARBPROC
common shared glClientActiveTextureARB          as PFNglClientActiveTextureARBPROC
common shared glGenerateMipmapEXT               as PFNglGenerateMipmapEXTPROC

'For shaders
common shared _shader100_                      as integer
common shared glCreateShaderObjectARB          as PFNglCreateShaderObjectARBPROC
common shared glShaderSourceARB                as PFNglShaderSourceARBPROC
common shared glGetShaderSourceARB             as PFNglGetShaderSourceARBPROC
common shared glCompileShaderARB               as PFNglCompileShaderARBPROC
common shared glDeleteObjectARB                as PFNglDeleteObjectARBPROC
common shared glCreateProgramObjectARB         as PFNglCreateProgramObjectARBPROC
common shared glAttachObjectARB                as PFNglAttachObjectARBPROC
common shared glUseProgramObjectARB            as PFNglUseProgramObjectARBPROC
common shared glLinkProgramARB                 as PFNglLinkProgramARBPROC
common shared glValidateProgramARB             as PFNglValidateProgramARBPROC
common shared glGetObjectParameterivARB        as PFNglGetObjectParameterivARBPROC
common shared glGetInfoLogARB                  as PFNglGetInfoLogARBPROC
common shared glGetUniformLocationARB          as PFNglGetUniformLocationARBPROC
common shared glUniform1iARB                   as PFNglUniform1iARBPROC
common shared glUniform1fARB                   as PFNglUniform1fARBPROC
common shared glUniform2fvARB                  as PFNglUniform2fvARBPROC
common shared glUniform3fvARB                  as PFNglUniform3fvARBPROC

'For anisotropic fitering

common shared as GlHandleARB Shader_Bump,  Shader_MultiBump
common shared as GlInt DecalLoc, NormalLoc
common shared as glint colormap1, colormap2, alphamap, normalmap
common shared _anisotropic_ as integer  
common shared as integer use_shaders, use_multitex, use_anisotropic



declare function Open_GL_Window( Byval W As Integer, Byval H As Integer, Byval BPP As Integer, Byval Num_buffers As Integer, Byval Num_Samples As Integer, Byval Fullscreen As Integer ) as integer' as extensions_t
declare function Init_Shader( File_Name as string, Shader_Type as integer )as GlHandleARB
declare sub Gather_Extensions()

'declare function load_texture( byref filename as string, Texture() as Texture_Struct, byref force_clamp as integer ) as integer
'declare Sub Build_Stencil_Cube( Byref List As Gluint )
declare sub Make_Font_DList( Byref DList As GlUINT, Byval Wtiles As Single, Byval Htiles As Single, Byval t_Scale As Integer ) 
Declare Sub GlPrint( Byval Strng As String, Byval Texture As Gluint, Byval Text_List As GLUINT, Byval X As Single, Byval Y As Single, Byval R As Single, Byval G As Single, Byval B As single )
declare sub Set_Ortho( Byval W As Integer, Byval H As Integer )
declare sub Drop_Ortho()


